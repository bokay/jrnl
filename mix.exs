defmodule Jrnl.Umbrella.Mixfile do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      start_permanent: Mix.env == :prod,
      deps: deps(),
      releases: [
	web: [
          include_executables_for: [:unix],
          applications: [
	    web: :permanent,
	    jrnl: :permanent
	  ]
	]
      ],
      version: "0.0.1"
    ]
  end

  defp deps do
    []
  end
end
