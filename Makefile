make_release:
	docker build --force-rm --no-cache  --build-arg MIX_ENV=prod --build-arg HOST=niko.bokay.io -f Dockerfile -t blog_prod .
	docker save -o blog_archive.tar blog_prod

push:
	scp blog_archive.tar admin@exdone.review:~/archives/latest_blog

run_release:
	ssh admin@niko.bokay.io make release_blog

deploy: make_release push run_release
