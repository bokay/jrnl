[Voir l'article original](http://www.edouardglissant.fr/toutmonde.html)

<p>
1997 marque dans la pensée de Glissant l'apparition d'un nouveau concept, celui du Tout-monde, qui fait l'objet à la fois d'un roman et d'un essai - savamment mêlés, comme on pouvait s'y attendre. Ce nouveau néologisme n'est pas une fantaisie, loin de là, puisqu'à lui seul, il opère la synthèse de tout l'infléchissement de cette pensée depuis le tournant des années quatre-vingt dix, où l'écrivain s'attache à penser l'interpénétration des cultures et des imaginaires. Le Tout-monde désigne ce faisant la coprésence nouvelle des êtres et des choses, l'état de mondialité dans lequel règne la Relation.
</p>

<hr />

<p class='center'>
<iframe width="560" height="315" src="https://www.youtube.com/embed/5_L2zD_BF1w" frameborder="0" allowfullscreen></iframe>
</p>

<hr />

<p>J'appelle Tout-monde notre univers tel qu'il change et perdure en échangeant et, en même temps, la "vision" que nous en avons. La totalité-monde dans sa diversité physique et dans les représentations qu'elle nous inspire : que nous ne saurions plus chanter, dire ni travailler à souffrance à partir de notre seul lieu, sans plonger à l'imaginaire de cette totalité. Les poètes l'ont de tout temps pressenti. Mais ils furent maudits, ceux d'Occident, de n'avoir pas en leur temps consenti à l'exclusive du lieu, quand c'était la seule forme requise. Maudits aussi, parce qu'ils sentaient bien que leur rêve du monde en préfigurait ou accompagnait la Conquête. La conjonction des histoires des peuples propose aux poètes d'aujourd'hui une façon nouvelle. La mondialité, si elle se vérifie dans les oppressions et les exploitations des faibles par les puissants, se devine aussi et se vit par les poétiques, loin de toute généralisation.</p>

