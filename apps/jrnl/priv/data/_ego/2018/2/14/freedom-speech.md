They couldn't believe <br />
a curious mind would connect neurons <br />
in such an intriguing way, <br />
to deliver such a corrupted thought. <br />
 <br />
As if, for all I had seen, <br />
and all I'd learned, <br />
and lived, <br />
and smashed together, <br />
thoughts became dangerous for humanity itself. <br />
 <br />
They locked me up, for thinking, <br />
They locked me up, for dreaming. <br />
They locked me up, for feeling. <br />
