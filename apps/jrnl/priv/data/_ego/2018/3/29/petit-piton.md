<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ZoLeQjjrUws?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

A few weeks ago, I went to my homeland, Martinique, for a week of recovery. It was a about three years that I didn't see the island.

On my way to visit my grand-father, I asked my father to hop to the place where I grew up. It's a place far from the many clichés you can ear about living on an island. That hill has my name: Morne Charlery.

That place is at the edge of the city of Carbet. If you go straight through the zion, you'll reach the "canal des esclaves". It's always surprising to ear the river from up there. The waterfall in front is quite exceptional. You can't guess how strong it becomes after the passing of a hurricane. And far away, towards the see, you can see the city of Morne-Vert. I've always wondered how it is to see my place from the other side.

Growing up there was a bit special. I remember people thinking it was sort of a curse to live there. Even though we were on a small island, it's a place where you can easily feel isolated from society. I could count the neighbors on three fingers, one being my own grand-father. Despite the loneliness, I had quite a joyful childhood at this place. And because Mother Nature was surrounding my every step, I grew with a vivid curiosity of the world. Though, my inner self forged by the noisy silence of the Forest.

I believed I could speak to my ancestors through to the mountain. That I could control the wind, and that it was a privilege to wake up in such a beautiful place. It sure was. 

My weekends were filled with all sorts of discoveries, insects, birds, trees, wind, tropical storms, snakes, volcanic rocks, farming, dogs... I was so curious about the world, that I ate books about science, geography, astronomy, chemistry and even computers. I loved science because of this place. 

My parents, on the contrary, struggled to make a living. I watched my father destroyed by the hard labour of being an honest farmer, and my mother doing her early steps in building the small company that will provide for my family.

I now live the city, but my heart belongs to that place, and that island, where the Gods granted me the Freedom of a happy childhood. 

An té ich neg'mawon ki an jou, té pou dessan' an ba mòn'-la.
