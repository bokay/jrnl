<blockquote lang='en' style="text-align: left !important">
You are a bit critical, guarded and inner-directed.<br />
<br />
You are dispassionate: you do not frequently think about or openly express your emotions. You are proud: you hold yourself in high regard, satisfied with who you are. And you are independent: you have a strong desire to have time to yourself. <br />
<br />
Your choices are driven by a desire for efficiency. <br />
<br />
You are relatively unconcerned with both tradition and taking pleasure in life. You care more about making your own path than following what others have done. And you prefer activities with a purpose greater than just personal enjoyment. <br />
</blockquote>

I've done the personality test with the IBM Watson AI, giving it access to my Twitter timeline, and the list of contacts I follow.
However, to my defense for this cold portrait, I would object that twitter represents only one aspect of my identity. A subset of my numerical identity. To portrait me well, it would require my identity as a husband, as a coworker, as a son, as Martinican, as a friend, a lover, and passionate for the "positive" ones.

<i>In fine</i>, AI can only dress a partial portrait for the data it can get, the data I'm disposed to share.

The test can be found [here](https://personality-insights-demo.ng.bluemix.net/) and I've discovered it via [Kottke](https://kottke.org/18/04/your-personality-according-to-ibm-watson).
