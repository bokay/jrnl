A few days ago, I saw this tweet about this girl stating that she will be the next Simone Manuel, first black woman winning a gold medal at swimming for USA, with literally stars in her eyes.

<div class="centered">
<span>
<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Don&#39;t tell me representation doesn&#39;t matter. After <a href="https://twitter.com/hashtag/SimoneManuel?src=hash">#SimoneManuel</a> won, this little girl said, &quot;I got next.&quot; <a href="https://twitter.com/hashtag/Rio2016?src=hash">#Rio2016</a> <a href="https://t.co/2BmVp8JNoA">pic.twitter.com/2BmVp8JNoA</a></p>&mdash; TyreeBP (@TyreeBP) <a href="https://twitter.com/TyreeBP/status/764107068567461889">12 août 2016</a></blockquote>
</span>
</div>

With this tweet and the recent TV movie I saw about the story of Gabby Douglas, I realized that I actually was hiding something that I should not hide: me being a decent developer. For a long time, I've hidden my face from my publications because very often, I've seen my opinions being judged differently when my colour was revealed.

When entering the job market in France, black students will think twice before putting a photo of their face that does not match with beauty standard on a Curriculum Vitae. That's what I did to find my first jobs, hiding my face in order to increase my chances to get the job for my skills and not for what I look like. I'd be mad to no even get to the interview.

On Internet, it's different from [the street](/categories/ego/posts/2016/03/04/mind-drop#introduction). A name does not carry a colour, nor a pseudonyme. But when I help someone on Internet with part of my identity hidden, I truely don't help the community.

So, thank you to that girl's father for making me realize that I can help too.
