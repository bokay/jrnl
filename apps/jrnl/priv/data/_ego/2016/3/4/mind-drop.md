Today,

- I helped the team delivering some important feature for the client
- I slept 10 minutes in the middle of the day, because I needed it to
- I also forgot eating
- Kendrick Lamar released his [album "untitled unmastered"](https://open.spotify.com/album/0kL3TYRsSXnu0iJvFO3rud)
- I've exchanged with Sasha Chua about [her awesome attitude](http://sachachua.com/blog/2016/03/microphthalmia-small-eye/) she has towards her new born's microphthalmia. A true hero, a mother.
- I greeted the hashrocker team for their [TIL website](http://til.hashrocket.com/), for sharing the best things they learned on a daily basis
- I've watched some bits of the [Strolling series]() about the different black identities, and some of the silly monologues that happen in my head.

----
<a class="anchor" href="#introduction" id="introduction" class="anchor">&#9875; </a>
## Introductions

Before talking to each other, let's take a minute, would you?

Here. Look at me now, entirely, if you dare.
Then reach my eyes and, <br/>
See my pride, my ego, <br/>
Perceive my strength, my vulnerabilities, <br/>
Consider my past as a human being, <br/>
Imagine the stories I've been through, <br/>
Anticipate my emotions, my reactions, <br/>
Guess how others look at me, <br/>
Ear the motions of my soul, <br/>
The peace of our conversation. <br/>

Once you know me well enough, let's finally introduce ourselves, using our voices.

-
`discutere ‎(“to strike or shake apart, break up, scatter, also, in derivatives and in Medieval Latin, examine, discuss”), from dis- ‎(“apart”) + quatere ‎(“to shake”).`

----

<a class="anchor" href="#drawing" id="drawing">&#9875; </a>
The first drawing I remember doing was a black rastaman sat on the pavement, begging for money. In front of him was a policeman staring at him, haughty. I was 13. <br/>
How can one be comfortable ignoring somebody begging for food or money in the streets. My guilt is never consumed if I give a dollar. <br/>

This is how I feel the world, my non-conformism with society started there.

----
<a class="anchor" href="#ayiti" id="ayiti">&#9875; </a>
I've learned today that the island of Ayiti was once the theater of [a war between the Spanish and the French](https://en.wikipedia.org/wiki/Battle_of_Verti%C3%A8res), and that the Dominican Republic celebrate it's independance day as when they succeed to split with the French part. <br/>
Imagine that... one people separated in two for the account of two colinialist countries fighting for power, land and slaves. <br/>
History say that massacres of black haitian people followed, being banned from Santo Domingo.

---
<a href="#misc" id="misc">&#9875; </a>
## Misc.

When Haiti got its independance from France, they had to pay the price for their freedom as a debt.

I wonder how those revenues are taken into account in the [French State budget registers](https://fr.wikipedia.org/wiki/Budget_de_l'%C3%89tat_fran%C3%A7ais) ?

---
<a href="#pause" id="pause">&#9875; </a>
## Pause

Want a shot of a taste of the canadian haitian identity ?
Discover the group [Makaya](https://open.spotify.com/track/1q98BKKNrSQ2K5tbrtSTjR).

<iframe src="https://embed.spotify.com/?uri=spotify%3Atrack%3A1q98BKKNrSQ2K5tbrtSTjR" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>

---
<a class="anchor" href="#history" id="history">&#9875; </a>
I can't stand how history books are made, telling only one side of a story. <br/>
When Napoleon restored slavery in the French Antilles, he was seen as a Hero. <br/>
He still is considered as so by white French historians. My people has a different story to tell you.

<iframe width="560" height="315" src="https://www.youtube.com/embed/bya2TRop7y4" frameborder="0" allowfullscreen></iframe>

Can we talk about the benefits my country earned while being a monster on the other side of the world ? <br/>

I like this idea of books enriched by [augmented reality](http://www.psfk.com/2016/03/lacoste-lt12-brand-culture-augmented-reality-book.html). Will it help us embracing the ambivalence of our world ?

There's a spectrum of stories around a single event.

---
<a class="anchor" href="#family" id="family">&#9875; </a>
Dear sister. <br/>
Dear wife. <br/>
Dear mother. <br/>
Dear father.

I love you so much.

---
<a class="anchor" href="#ojala" id="ojala">&#9875; </a>
I often struggle speaking out loud for a simple reason. <br/>
On computer science, when we want to access an information very often, we cache it in a place so it can be accessed faster, cheaper, without deep algorithm, nor energy. <br/>
My mind does the same. <br/>
My thoughts are so complex, and I often want to find the very word that will express my thinkign perfectly. As I'm not a chatty person, that dictionnary of words get dust and finding a word require from me reconnecting some neurons. <br/>

I know how to fix that : socialize me, make me speak !

I deeply love learning the etymology of words. How a word has been thought with time to express a something. What it was refering back in the time, how we use it now. The time that shaped it.

Did you know that Ojala in Spanish comes from Insha'Allah in Arabic. How beautiful is that ?

---
<a class="anchor" href="#obama" id="obama">&#9875; </a>
I have a good opinion of Obama's speeches, but I can't stand the clownesque part of him when he tries to get the opinion to his favor. <br/>
Politics shouldn't be about promoting yourself.

If I were American, you wouldn't get my second vote, sadly.

---
<a class="anchor" href="#microagressions" id="microagressions">&#9875; </a>
Microagressions.
I've learned this word today and I didn't know how to those things every black person are subject in their daily life. <br/>
When I arrived in Montreal, I went to the closest grocery store and examine everything so I knew better what I could buy when hungry. I felt the pressure of the owner, 'cause you know... I could be a thief ! <br/>

I didn't get engineer because of money but more for recognition. That's what I'd throw at people being condescendant, guessing my identity on statistics.


PS: I also got the engineer degree because it was fun and for money expectations. How human is that ?!

---
<a class="anchor" href="#identity" id="identity">&#9875; </a>
There's no one black identity. <br/>
Our histories and personal stories are so different.

<iframe width="560" height="315" src="https://www.youtube.com/embed/mCP-5dyPIuU" frameborder="0" allowfullscreen></iframe>

---
<a class="anchor" href="#caring" id="caring">&#9875; </a>
I love sharing my knowledge. <br/>
And I love learning things. <br/>
That's why I'm a [human|software coder].

---
<a class="anchor" href="#empathy" id="empathy">&#9875; </a>
Empathy should be taught at school. <br/>
Many conflicts can be avoided with empathy.

---
<a class="anchor" href="#change" id="change">&#9875; </a>
I read a good portion of the book ["Du désir au plaisir de changer"](http://www.francoise-kourilsky.com/goFolder.do?p=menu_d6d3c0fafe5440fe&f=58bd7a1724c34881) by Francoise Kourislky, talking about human interactions, and the book [Ishmael](https://en.wikipedia.org/wiki/Ishmael_%28novel%29) by Daniel Quinn.

They both share that lesson : the importance of questioning to help sharing a point of view, or to reach a consensus.

See the difference between :
`You're wrong, you should do it that way.`
and
`Can you explain me why did you do it that way ? Shouldn't it be better if we did it that way ?`

This is one thing that I love about small teams : the opportunity to instore a dialogue, explore ideas rather than imposing an idea.

---

Let's get some rest now. <br/>
Maybe will we connect each other somewhere in our so different but human dreams :)
