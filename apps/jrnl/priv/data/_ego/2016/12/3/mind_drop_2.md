This week,

- We've been through health issues with my better half
- I joined back Facebook
- I've watched The Throne and a documentary about urban farming in Detroit

----

<a class="anchor" href="#mccalla" id="mccalla" class="anchor">&#9875; </a>

- Leyla McCalla - Peze Café
- Leyla McCalla - Mesi Bon dié

----

<a class="anchor" href="#poetry" id="poetry" class="anchor">&#9875; </a>
Poetry, <br />
Way before being words and marks, <br />
Is voice and tone. <br />
Poetry reaches the hearts, <br />
When both poets and readers follow same pace, <br />
the same tone, get to the same emotion.

----

<a class="anchor" href="#letitgo" id="letitgo" class="anchor">&#9875; </a>
Let it go.

Dear daughter, dear son, <br />
That is the secret of a happy life.

But keep what is truely yourself.

----

<a class="anchor" href="#byebye" id="byebye" class="anchor">&#9875; </a>
The French president has decided not seeking for a second term. Yet, some were shocked as if what's considered normal is to go for a second term by all mean. <br />
Do we really deserve a democracy?

----

<a class="anchor" href="#africa" id="africa" class="anchor">&#9875; </a>
<a href="https://twitter.com/Voodart">Audrey N'Gako</a> makes me want to <a href="https://www.instagram.com/visiterlafrique/">travel Africa</a>.

----

Forgiveness.
