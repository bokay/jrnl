Au gré de ma navigation dans les eaux du Web, je tombe sur cette copie d'un [« Dictionnaire Synoptique d'étymologie Française »](https://books.google.ca/books?id=y-cSAAAAYAAJ&lpg=PA434).

A la recherche de l'origine de l'expression employée par Aimé Césaire dans son « calendrier lagunaire » dont je vous invite à découvrir [la magnifique interprétation de Nicole Dogué](https://open.spotify.com/track/7cgTXXhI5pbxndn9b2OFNv), je tombe sur cette définition :

> « Anglo-Sax. VRAC (à), qqch. de rejeté; Angl. Wreck, débris de navire. VARECH ou Varec, fucus, plante marine que la mer arrache en montant et jette sur le rivage ; — navire coulé, débris quelconques rejetés par la mer. VRAC (corruption du mot varec, le varec ne demandant aucun arrimage dans les bateaux qui le transportent!). État des marchandises que l'on jette péle-méle dans le navire, sans donner aucun soin à leur arrimage. Mode d'expédition des marchandises non emballées : meubles expédiés en vrac. »

Sur la première de couverture, on peut y lire ceci:
<cite>« On peut dire absoluement que personne n'a le droit de proférer le mot avec autorité, que personne n'a le droit de lui donner sa valeur virtuelle, sans en connaître l'étymologie et la prononciation. » — Ch. Nodier.</cite>

Alors je me mets à penser à cette histoire d'accent circonflexe que l'on voudrait voir disparaître, et je me rappelle des mots qu'il m'aura permis de découvrir, deviner ou même traduire en le remplaçant par son aîné, le « s ».

<figure>
<img src='/assets/ego/2016/2/14/vrac-de-varec.jpg' alt="Gribouillis, montréal, qc" title="Gribouillis, montréal, qc"/>
<figcaption>Gribouillis en vrac d'une façade de café, rue Beaubien, Montréal, Canada</figcaption>
</figure>
