Je me mis à contempler ce grand planisphère.  
Puis, je méditai que les incidents de la semaine passée,  
région après région, pays après pays.  
Leurs conséquences et leurs causes.  
Je pris conscience de l'équilibre du monde.  
Du chaos, des horreurs et des joies,  
des frictions qui ont un parfum d'éternité.  
Le futur s'écrivait.  
