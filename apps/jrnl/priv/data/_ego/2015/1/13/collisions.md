![Manifestation à Montréal](/assets/ego/2015/1/13/montreal_je_suis_charlie.png)

Ce mois de janvier 2015 a marqué le monde et le peuple français. Tous les yeux du monde étaient rivés sur des écrans présentant des faits, une réalité du moment, mais en omettant pour beaucoup de présenter le contexte politique, social et diplomatique dans lequel il s’est produit.

J’aime à penser que nous êtres humains, ne sommes que de vulgaires cellules géantes autonomes, partageant un ADN commun mais ayant notre propre singularité. Et je poursuivrai avec ce biomimétisme.

Tout virus croit dans un contexte et un environnement qui lui est propre. Nos défenses immunitaires également liées à qui nous sommes, ce que nous mangeons et où nous évoluons. C’est pareil pour les maux de nos sociétés. Si ces attaques terroristes sont la partie visible d’une succession de faits, il faut nous interroger à la fois sur les causes de ces attaques mais aussi sur la qualité de l’environnement dans lequel elles ont pu se produire.

Pour moi, le terreau de ce drame réside dans l’enclavement des cités, l’étroitesse financière d’une classe entière de travailleurs acharnés, la [violence de l’environnement carcéral](http://www.slate.fr/grand-format/anders-breivik-prison-doree)[(cache)](/categories/cache/posts/la-prison-humaine-norvegienne), le système éducatif en manque de moyens et une décentralisation des régions trop pauvres.

On peut s’imaginer quelle a été la vie de ceux qui sont passés à l’acte. Quelles ont été leurs déceptions, et où la société, le sang de la république, a échoué à leur montrer le chemin.

Le monde occidental a sursauté, prenant conscience qu’un mal le rongeait, au plus profond de ses entrailles, le mettant face à ses paradoxes. Il avait fait le choix de ne pas manger sainement, courant après une croissance à tout prix, ignorant le reste du monde, faisant l’autruche sur son état de santé.

Son alimentation n’était pas saine, et cela des années durant.
