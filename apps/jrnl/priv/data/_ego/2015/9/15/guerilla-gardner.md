<blockquote>
Growing your own food is like printing your own money !
<cite lang='en'> <a href='http://www.ted.com/talks/ron_finley_a_guerilla_gardener_in_south_central_la#t-253379'>Ron Finley: A guerilla gardener in South Central LA</a></cite>
</blockquote>

(via [Risebox](https://twitter.com/getrisebox/status/643411164957945856))
