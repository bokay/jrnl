Je compte passer par eux pour mes prochaines commandes de semences :
[https://www.seeds-organic.com/](https://www.seeds-organic.com/)

Je suis également tombé sur ce site aux allures des années 90 mais qui regorge de bonnes informations sur la germination des graines consommables :
[http://www.cfaitmaison.com/germs/graines4.html](http://www.cfaitmaison.com/germs/graines4.html)

Un régal pour les sandwishes fait-maison !

J'aimerais un jour arriver un jour à effectuer des rotations de cultures indoor.