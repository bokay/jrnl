Ce billet fait suite à la question <a href='https://larlet.fr/david/stream/2015/11/15/'>pourquoi écrire ici</a> de David. Je me suis cherché une réponse toute aussi personnelle.

<blockquote lang='fr' cite='urn:isbn:2070132188'>
Écrire la vie. Non pas ma vie, ni sa vie, ni même une vie. La vie, avec ses contenus qui sont les mêmes pour tous mais que l'on éprouve de façon individuelle : le corps, l'éducation, l'appartenance et la condition sexuelles, la trajectoire sociale, l'existence des autres, la maladie, le deuil. Je n'ai pas cherché à m'écrire, à faire œuvre de ma vie : je me suis servie d'elle, des événements, généralement ordinaires, qui l'ont traversée, des situations et des sentiments qu'il m'a été donné de connaître, comme d'une matière à explorer pour saisir et mettre au jour quelque chose de l'ordre d'une vérité sensible.
<cite> Écrire la vie,  Annie Ernaux </cite>
</blockquote>

Le matin, au réveil, mon premier geste sera d'ouvrir mon aggrégateur de flux à la catégorie <em>Citoyens</em>. J'y ai regroupé des flux RSS d'ici et d'ailleurs, de personnes authentiques, s'exprimant à la première personne. Ils ont le points commun de s'autoriser le doute dans leurs écrits.

J'y lis des opinions, rêve des voyages, explore des techniques. Je pense et je vis des vies, qui ne m'appartiennent pas, que je ne croiserai certainement pas mais qui m'aideront à faire les choix de demain. J'envisage mes prochains pas en lisant les leurs.

<figure>
<img src='/assets/ego/2015/11/18/grange.jpg' alt="Grange sur l'île d'Orléans, QC" title="Grange sur l'île d'Orléans, QC"/>
<figcaption> Québec, 9 Juillet 2015 - Grange sur l'île d'Orléans </figcaption>
</figure>

Dimanche dernier, je me suis laissé aller à une activité que j'ai longtemps aimé à Paris : passer des heures chez un marchand de livre. Je serai le premier à regretter la fermeture d'un endroit comme la FNAC de Chatelêt-les-Halles, ou Renaud-Bray ici à Montréal. J'y ai cherché les auteurs qui pourraient m'aider à écrire. Alors je me suis demandé ce que j'aime lire. <br>
D'Annie Erneaux à Nicolas Bouvier, des noms croisés au détour d'un blog, j'entrepris de trouver ce qui me plait tant à lire les écrits de nomades, ou contempler les photographies de rue.

De fil en aiguille, j'en viens à conclure que j'aime leur faculté à témoigner et tracer le quotidien; à écrire le présent, écrire la vie.
A l'opposé, un roman en général m'ennuie.

Alors j'écris moi aussi parce que je vis.<br />
Pour moi, d'abord. <br />
Et ici, parce que <a href='https://larlet.fr/david/blog/2015/oiseau-bleu/'>comme toi David</a>, je rêve d'un <span class='strike'>monde</span> web plus libre.