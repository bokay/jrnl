
<blockquote lang='fr' cite='urn:isbn:9788420416564'>
Qui serait assez insensé pour mourir sans avoir fait au moins le tour de sa prison ?
<cite>L'Oeuvre au Noir</cite>
</blockquote>

D'ailleurs, quelle est ma <q lang='fr'>prison</q> et comment se définit-elle ?<br/>
Quels recoins de confort m'éloignent d'une vie libérée ?