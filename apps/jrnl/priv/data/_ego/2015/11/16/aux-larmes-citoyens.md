Les yeux rivés sur le petit écran. L'effroi à la connaissance des événements. Une tension certaine. Quelques questions me taraudent. Ces hommes sont déterminés. L'esprit gangréné par une certaine idéologie. Une vision du monde, leur idéal.

Alors, si j'estime que ma vision du monde est meilleure, pour moi et pour les miens, existe-t-il une négociation/discussion possible ?

Et si, à vouloir s'imposer, une idéologie quelle qu'elle soit, entraîne la mort d'innocents, alors comment arrête-t-on une idée ?

- par la science et l'éducation;
- en posant des contre-arguments plus forts et convaincants;
- par l'humour, la dérision et l'absurde;
- par l'acceptation d'inconnues et approximations.

En écrivant cette possible énumération, je me rends compte de l'aspect mathématique de nos échanges. De la complexité à résoudre la plus dure des équations : <em>le dialogue</em>.