<figure>
<img src='/assets/ego/2015/11/24/workers.jpg' title='Ebenistes refaisant un balcon' />
<figcaption>
Montréal, 24 novembre 2014
</figcaption>
</figure>

Les premiers flocons ne perturbent pas les travailleurs. <br />
<hr/>

Et moi qui travaille de la maison, je me demande : est-ce bien juste tout ça ? <br />
Et si le travail se définit comme un lieu, est-ce qu'il m'arrive d'arrêter de travailler ?
Ou au contraire, si je choisis mon confort, est-ce vraiment du travail ?

Et pourtant, je le sens le labeur.
