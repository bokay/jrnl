<blockquote lang='en'>
When you can work despite the hardships, when you can see the opportunity in the middle of catastrophes, and when you can stay positive even when the world is pouring down on you, maybe that is what can be called happiness
<cite><a href="http://erickimphotography.com/blog/2015/10/22/streettogs-academy-15-happiness-results-and-analysis/">Eric Kim, “Happiness” Results and Analysis</a></cite>
</blockquote>

<figure>
<img src='/assets/ego/2015/11/19/happiness.jpg' />
<figcaption>Montréal, 14 juillet 2015</figcaption>
</figure>

Je traversais l'allée quand mon regard s'est posé sur cette rénovation de façade. <br />
J'étais avant tout intrigué par la réalisation, mais au moment où je me positionnais pour prendre la photo, un des travailleurs a prévenu ses collègues pour prendre la pose.
Dans leur labeur, j'avais capturé un moment de bonheur, et j'y ai pris ma part.

:)
