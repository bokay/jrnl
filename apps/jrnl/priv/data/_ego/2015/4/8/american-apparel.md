<div class='center'>
<figure>
  <img src='/assets/ego/2015/4/8/american_apparel.jpg' title='Montréal, 3 mars 2015 - American Apparel' />
  <figcaption> Montréal, 15 mars 2015 - American Apparel </figcaption>
</figure>
</div>

Ce message visible de loin sur la rue Mont-Royal ne passe pas inaperçu.   

En creusant, j'apprends que cette entreprise, jadis fondée à Montréal et localisée maintenant à Los Angeles, se fournit en coton bio, s'alimente en énergie solaire, recycle ses propres déchets et même s'engage auprès d'associations pour les immigrants et les homosexuels. Le tout en se prônant fièrement une fabriquation made in LA.

*Cependant, elle reste une entreprise dont le principal but est la croissance.*

N'aurait-elle pas été plus éthiques si elle était restée à une échelle locale ou au pire nationale?  
Les valeurs affichées, qui représentent les valeurs de la maison mère, sont-elles respectées auprès des distributeurs nationaux et internationaux?  
Car oui, je doute qu'il y ait un panneau solaire sur le toit de cet immeuble !

Dommage que ces valeurs, et notamment le principe du pollueur-payeur, ne soient pas plus répandues cela dit.
