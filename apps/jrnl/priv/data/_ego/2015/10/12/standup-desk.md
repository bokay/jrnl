## In action
<img style='max-width: 45%; display: inline' src='/assets/ego/2015/10/12/stand1.jpg' />
<img style='max-width: 45%; display: inline' src='/assets/ego/2015/10/12/stand2.jpg' />
<img style='max-width: 45%; display: inline' src='/assets/ego/2015/10/12/stand3.jpg' />
<img style='max-width: 45%; display: inline' src='/assets/ego/2015/10/12/stand4.jpg' />
<img style='max-width: 45%; display: inline' src='/assets/ego/2015/10/12/stand5.jpg' />

## The setup
* **The desk : [TORNLIDEN / FINNVARD](http://www.ikea.com/ca/en/catalog/products/S79047288/) from IKEA** <br />
** Architect or drawing desk. <br />
** Can me inclined, but I don't use that feature. <br />
** Manually ajustable with the help of someone else. A bit tideous.

* **The bar stool : [SELENA](http://www.zonemaison.com/meubles/tabourets/48065) from Zone** <br />
** Back of chair in sat position. Ideal to rest the back. <br />
** High enough <br />

## First impressions
I can quickly switch between positions, and always have the possibility to rest.
Also, I can get in the room and be already in war position to attack a bug or a feature.
However, I will certainly rise up the monitor so I don't have to lower my head. I've tried this setup at work and I absolutely love it.
