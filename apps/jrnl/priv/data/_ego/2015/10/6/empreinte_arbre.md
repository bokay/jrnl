<img alt='empreinte_arbre' title="Empreinte d'une tige sur un arbre" src='/assets/ego/2015/10/6/empreinte_arbre.jpg' />

J'ai été attiré par le mouvement de la tige qui suit celui du tronc et le rapport de force et de temps qui les oppose.