Ce weekend, j’ai fini de regarder la série [The Book of Negroes](http://www.cbc.ca/bookofnegroes/).

<figure>
<img src="/assets/ego/2015/2/22/the_book_of_negroes_aminata.jpg" title="Aminata Diallo, jeune, prisonnière de marchands d'esclaves, et qui traversera bientôt The big River" />
</figure>

J’ai été très ému par cette histoire d’une femme noire dérobée de son Afrique natale, mise en esclavage en Amérique du Nord, ayant fuit en Nouvelle-Ecosse et parvenant à réaliser son rêve ultime.
Cette histoire me parle et parlera à tout descendant d’esclave.  Chaque image me prenant aux tripes et me ramenant à cet ancêtre qui a vécu cette traversée, qui a rêvé s’enfuir mais qui, hélas, est resté prisonnier.

Être issu de la diaspora noire et évoluer dans le [Tout-Monde](http://www.edouardglissant.fr/toutmonde.html)[(cache)](/categories/cache/posts/tout-monde).
ne se fait pas sans heurts ni blessures. En France, il faut faire face à un racisme dissimulé. Pour moi, ça a été un contrôle au faciès, se faire suivre par les vigiles dans les magasins, subir les blagues innocentes et racistes de camarades de classes en école d'ingénieur, voir en face de soi des gens ayant peur de dire *« noir »* par peur de vexer et se réfugiant sous le terme *"black"*. La discrimination a une attitude, un regard et même un langage.

Alors face à ça, plusieurs options se présentent. J’ai pu passer par plusieurs de ces étapes. On peut se présenter en victime et accuser l’autre de son malheur quotidien. On peut également se radicaliser et prôner la négritude à tout va. Pour moi, ça a été une étape nécessaire dans ma construction, mais voilà, pour vivre avec les autres, il faut savoir mettre de l’eau dans son vin. J’entends par là, connaître précisément son passé, reconnaître les actes et paroles à condamner, mais ne pas en faire un dictat. J’en ai vu qui, rongé par l’Histoire et des rancoeurs, ont finalement sombré dans une idéologie raciale et raciste.
Et il y a finalement la position qui vise à éviter de se présenter en victime. Celle-ci requiert du courage et de la sagesse. C’est un effort de tous les jours, et de chaque instant.

Ces postures s’appliquent à toutes les formes de communautés discriminées: femmes, homosexuels, invalides, quiconque appartenant à une minorité. Bref, n’importe qui n’entrant pas dans un moule pour une époque, une position géographique et une population donnée.

**De la difficulté d’être : de vivre la relation avec l’autre et de se préserver.**
