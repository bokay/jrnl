La Martinique est une île caribéenne avec des institutions françaises

Elle mimique très bien le fonctionnement français au point d'en récupérer également ses défauts organisationnels.

Elle dispose d'une « capitale », Fort-de-France, qui centralise toute l'activité autour des villes qui l'entourent. Elle dispose de quelques grands axes routiers, tous convergent vers la capitale.
Autrefois, c'est la ville de Saint-Pierre qui remplissait ce rôle, mais avec l'éruption de 1902 et pour éviter de tout perdre à nouveau, le chef-lieu de l'île a été délocalisé vers Fort-de-France.

Les problèmes sont les mêmes qu'à Paris : 

- des embouteillages monstres avec des pics suivant l'horaire;
- une concentration des entreprises sur la capitale, empêchant de ce fait l'existence d'autre pôles d'activités;
- les centres institutionnels et administratifs concentrés vers cet endroit;
- la pauvreté se concentre à la périphérie de la ville.

C'est bien là la caricature du modèle français, à tel point, qu'on en oublierait presque que l'on se trouve sur une île.
