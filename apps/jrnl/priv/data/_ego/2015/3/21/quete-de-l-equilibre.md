<div class='center'>
<figure>
  <img src='/assets/ego/2015/3/21/beloperone.jpg' title='Montréal, 3 Janvier 2015 - Bélopérone' />
  <figcaption> Montréal, 3 Janvier 2015 - Bélopérone </figcaption>
</figure>
</div>

Fils d'une lignée d'agriculteurs, je souhaitais devenir agronome étant plus jeune. J'ai choisi l'informatique, mais j'ai gardé l'amour du vert-nature.
Avant que mon père ne quitte cette terre, je tâche de discuter avec lui de l'essentiel. Mon père, c'est ce gaillard d'1m80, pas faignant pour un sou, droit et de principe, comme bien des agriculteurs. Des principes qui m'ont éduqué et que je transmettrai.

J'ai pu l'interroger sur sa pratique de l'agriculture et son utilisation des pesticides au moment où il m'a élevé. Il n'a pas eu le choix: *«Quand une maladie peut s'emparer de ta récolte, que tu as une famille à nourrir, et que peu de gens s'intéressent aux laitues sans pesticides, tu n'as pas le choix. C'est soit ça ou mettre la clé sous la porte»*. Cependant, il m'a aussi expliqué qu'il n'a pas eu recours aux pesticides systématiquement comme c'est, hélas, devenu une habitude dans les pratiques agricoles actuelles, mais qu'il les utilisait avec parcimonie quand cela était absolument nécessaire.
Les lois du marché sont cruelles.

Aujourd'hui, les pratiques évoluent, on observe un retour aux produits sans pesticides, et également un retour à des pratiques plus traditionnelles comme [les jardins créoles](http://www.inra.fr/Grand-public/Agriculture-durable/Toutes-les-actualites/Jardin-creole)[(cache)](/categories/cache/posts/jardin-creole-modele-agroecologie). Avec les dérives du chlordécone, ont-ils vraiment le choix?

Et lisant [Karl](http://www.la-grange.net) sur le [confort d'être éthique](http://www.la-grange.net/2014/09/07/capacite), cela me conforte dans l'idée que pour suivre des valeurs éthiques, il faut d'abord une certaine assise, expérimenter de ses erreurs et ensuite viser un équilibre des pratiques. Les exemples ne manquent pas :

- l'évolution des énergies fossiles allant vers des énergies renouvelables;
- l'épanouissement des systèmes décentralisés;
- ou encore des politiques de dictat et corruptions s'assouplissant vers des démocraties, fermées, puis ouvertes.

<p><div class='center'><div>La vie est d'abord compétition.</div> <div>Elle cherche à survivre.</div> <div>En enfin, elle finit par trouver son équilibre.</div> </div></p>
