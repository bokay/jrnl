
<blockquote>
 L'amour est enfant de bohême, <br />
 il n'a jamais, jamais connu de loi ; <br />
 si tu ne m'aimes pas, je t'aime ; <br />
 et si je t'aime, alors prends garde à toi !
</blockquote>


Chaque année, depuis 3 ou 4 ans, se tient un concert gratuit d'opéra en plein air au Stade Olympique de Montréal. <br>
Cette année, l'opéra choisi a été Carmen.
La scène était belle, l'instant presque magique. Cet opéra a su captiver l'attention des uns, mais aussi l'indifférence de beaucoup. Il a tout de même réuni quelques 45 000 âmes.

Juste avant cet opéra se tenait un hommage à Hiroshima. <br />
L'instant a commencé avec des discours de paix et de recueillement. Puis, ils firent retentir des cloches pendant une minute,
en parfaite synchronisation avec la ville d'Hiroshima. Il faut savoir qu'à cet instant, toute la ville d'Hiroshima reste figée,
en hommage à ce jour, où quelques 140,000 âmes se sont brusquement envolées.

Je me tenais en retrait, vers les places les plus reculées du spectacle. Des agents de sécurité faisaient leur travail et ont décidé, devant l'exceptionnelle affluence, de ne pas laisser entrer tout le monde. La solennité de l'instant a été perturbée par cette foule qui voulait y assister. <br />
Chaque mouvement de la foule bafouait la mémoire des 140,000 âmes.
Le contraste était saisissant et quelque peu effrayant. D'un côté, une foule de gens assis, assoiffés d'empathie, s'imprégnant de chaque mot du poême du maire d'Hiroshima.
Et cette même foule, manquant de sympathie pour les laissés-pour-compte, réclamant avec raison, le silence et le respect que mérite l'instant.

C'est ainsi que j'ai réalisé ô combien la paix dans ce monde reste fragile.
Je me suis aussi dit que les cours d'histoire ne nous apprenent pas à ressentir la douleur des autres. L'empathie devrait être enseignée à ceux qui ne la percoivent pas.
Cet enseignement pourrait aussi faire qu'elle ne se perde pas avec le temps, de par la dureté de ce monde.

**Et que ces 140,000 âmes ne disparaissent jamais de notre mémoire collective.**

<div class='center'>
<figure>
<img alt="La foule au stade Olympique de Montréal pour le concert de Carmen" src='/assets/ego/2015/8/15/carmen-stade_olympique.jpg' />
<figcaption> Montréal, 5 août 2015 - Concert opéra Carmen au stade Olympique -
Crédit photo : <a href='http://parcolympique.qc.ca/nouvelles/2015/08/le-parc-olympique-presente-kent-nagano-et-lorchestre-symphonique-de-montreal-sur-lesplanade-financiere-sun-life/'>parcolympique.qc.ca</a></figcaption>
</figure>
</div>
