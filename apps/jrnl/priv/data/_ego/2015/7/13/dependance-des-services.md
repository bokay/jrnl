J'entame en ce moment une cure de désintoxication de Facebook. 

Le mot cure est un peu mal approprié car je vis en ce moment ressemble plus à une lassitude en terme de contenu. Trop de contenu que j'estime être du spam, des chaînes modernisées. Chacun montre ses valeurs en partageant sur son mur sans opinion propre. Ensuite, trop de nudité à tout bout de champ et un jeu de Scrabble diablement addictif et péniblement lent.

En tentant de rompre avec Facebook, mon principal frein a été plutôt surprenant : Spotify. J'aime voguer sur Spotify, d'artistes en artistes, de découvertes en découvertes. Ca m'arrive même de partager des morceaux avec des proches. Evidemment, je dispose d'un compte payant. Je le vis comme une redevance et c'est avec plaisir que je paye pour ce service rendu. Dommage que ce ne soit pas un service d'état.

Quand j'ai créé mon compte spotify, j'ai choisi, par commodité, de le faire via mon compte Facebook. Le confort a un prix. Maintenant, si je veux profiter uniquement de Spotify, je dois forcément [garder un compte Facebook actif](https://support.spotify.com/ca-fr/problems/#!/article/Facebook-deactivated/?in=search).

En fouillant dans l'aide en ligne, il n'y a aucun moyen d'exporter ses playlists pour les importer sur un compte neuf bénéficiant d'une authentification par email/mot de passe. Impossible non plus de modifier le moyen d'authentification sur le compte existant.

Quand les services dépendent d'autres services non perennes dans le temps, ce sont des clients que l'on perd. 
