<blockquote>
Tout homme aspire à etre libre et c'est là une grande et belle ambition, car que vaut la vie d'un prisonnier ou celle d'un esclave ? Il existe toutefois de nombreuses formes de prison ou de servitude. La plus subtile et la plus permicieuse, celle que bien peu d'hommes considèrent et dénoncent, c'est la prison intérieure de l'homme esclave de lui-même.
<cite>L'âme du Monde, Frédéric Lenoir </cite>
</blockquote>

Il était temps !

Après 4 ans sur OSX, j'ai enfin installé une Debian Jessie sur vieux macbook pro de 2011.
J'ai appris à vraiment développer sous Linux à l'IUT : C, C++ et JAVA. Des années riches en découvertes et en bidouilles, à la fois logicielles que matérielles. Et puis, il y a eu cette application OSX que je voulais absolument tester et j'ai sauté le pas car un ami qui travaillait chez Pixar avait pu m'avoir une réduction qui ne se refuse pas.
Au bout d'un certain temps, je me suis rendu compte que mes applications principales étaient open-sources :

- *Firefox :* je consomme le web;
- *Jitsi :* j'appelle la Martinique et la France via une ligne Freebox parisienne, et ce, gratuitement depuis Montréal. Merci Free ! J'utilise également le protocole XMPP pour communiquer de façon privée avec ma moitié;
- *Thunderbird*;
- *Vim/Emacs :* mes outils de travail.
- *iTerm :* évidemment, trouver un bon terminal sous linux est un jeu d'enfant;
- *Syncthings :* j'ai remplacé BTsync par Syncthings il y a quelque temps à cause des limitations imposées en début d'année par ce logiciel et l'opacité de son code source, bien que BT inspire confiance. La transition s'est faite sans encombres.

Pour les applications non-libres, j'ai pu trouver des alternatives satisfaisantes :

- *Shotwell :* je ne pensais pas pouvoir trouver un bon remplacant pour l'application Photos mais la simplicité et l'efficatité de Shotwell est redoutable pour des opérations aussi simples qu'une modification d'exposition, de contraste, de recadrage, etc.
- *Spotify :* j'ai dû résilier mon compte en [quittant facebook](/categories/ego/posts/2015/07/13/dependance-des-services) mais je compte me recréer un compte. J'ai de la chance car il existe un client Linux qui répond à mes besoins.

Comme dirait [Yannick](http://elsif.fr) : **"La route est longue, mais la voie est libre !"**