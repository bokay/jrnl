C'est à cet endroit que j'ai voulu passer le cap des 30 ans. Marquer la fin d'une époque et le début d'une nouvelle.

J'ai pu faire cette randonnée avec mon père. Un moment de partage de plus en plus rare à cause de la distance qui nous sépare, alors je l'ai voulu de qualité.

C'est une chose que j'aimerais continuer de faire une fois père.

<img src='/assets/ego/2015/7/12/la-route-des-montagnes.jpg' />
<br/>
<img src='/assets/ego/2015/7/12/les-montagnes.jpg' />
<br/>

Le lieu: Les hautes gorges de la rivière malbaie, Québec.
