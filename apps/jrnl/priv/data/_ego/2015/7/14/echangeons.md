<blockquote>
<div>« On the Internet, everybody can have a channel, anyone can get a blog or a myspace page.</div>
<div>Everyone has a way of expressing themselves.</div>
<div>An so what you see now is not a question of who gets access to the airwaves, its a question of who gets controls over the way you find people.</div>
<div>You know, you start to see power centralizing sites like Google, sorts of gatekeepers, telling you where on the internet you want go, the people who provides you your sources of news and information.</div>
<div>So it’s not only certain of people who have a license to speak.</div>
Now everybody has a license to speak, it’s a question of who gets heard. »
<div><br/></div>
<cite>Aaron Swartz</cite>
</blockquote>

Première journée sans Facebook et déjà les effets se font sentir. Les idées germent.

Il y a quelques jours, je constatais [la différence de sérendipité entre les différents de réseaux sociaux](/categories/ego/posts/2015/06/30/creer_du_lien). En rompant avec Facebook, j'ai également rompu des liens sociaux. Les échanges, les partages se feront autrement, ou ne se feront pas.

En analysant les échanges qui se font actuellement au sein de la blogosphère, je me suis aperçu d'une chose: ils sont faillibles. Je vois Jean répondre à Pierre sur son blog, qui lui même cite Arthur dans ses articles. 

**Quels moyens ont-ils de savoir qu'un autre site fait référence à leurs articles ?**
**Comment être notifié qu'un lien existe à l'autre bout de la toile ?**

Par ordre de fiabilité, aujourd'hui, on peut:

- envoyer un message privé pour notifier la personne dont on fait référence;  
> **Inconvénient:** il faut être joignable et laisser ses coordonnées. Cela dépend également de l'envie d'engager un contact. L'échange est actif et volontaire.

- écrire un commentaire pour notifier la personne dont on fait référence;  
> **Inconvénient:** tous les blogs n'implémentent pas des systèmes de commantaires. Par négligence ou par choix. 

- implémenter une des technique de [rétrolien](https://en.wikipedia.org/wiki/Linkback) (pingback, trackback, refback ou webmention);   
> **Inconvénient:** Si les blogs n'implémentent pas les mêmes techniques ou qu'un site tombe ou change d'URI, l'échange tombe à l'eau.

- suivre les RSS des interlocuteurs potentiels.   
> **Inconvénient:** Il faudrait suivre l'ensemble de la blogosphère pour être tenu au courant de tous les échanges. 


## Proposition:
Un robot pourrait crawler les flux RSS et via un service web, une URI disposerait d'un flux RSS de notifications. Ainsi, il serait plus aisé de suivre ses propres échanges mais également les échanges publics de ceux qui nous intéresse.

## Avantages:
- L'échange est passif. On peut ne pas souscrire au flux RSS et ignorer l'échange.  
- L'échange est retrouvable dans le temps.
- La seule condition technique est que les deux blogs participant à l'échange implémentent un flux RSS ou Atom, technique très largement répandue.

## Inconvénient:
- c'est un robot (sic!)
- la première implémentation qui me vient à l'esprit est fortement centralisée, mais reposerait sur un format ouvert, le RSS. On peut cependant imaginer que la base de données soit sur un support distribué, comme du P2P.
