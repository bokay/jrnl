<img src='/assets/ego/2015/6/30/jardins_potagers.jpg' />

En développant cette photo, j'ai été surpris d'y voir apparaître [un lien](http://www.joatu.org), d'autant plus que j'apprécie le mouvement autour de l'agriculture urbaine à Montréal.

J'engage depuis quelques temps une reconquête de mon [identité numérique](http://niko.bokay.io/categories/dev/posts/ipv6-gandi-et-bokay-mwen).
Après avoir archivé toute ma timeline de Facebook, j'ai décidé de supprimer une à une, tous mes publications présentes sur Facebook. Une à une. J'ai libéré mon espace, supprimé ces traces de mon passé et me suis amusé de voir les liens que j'ai pu créer, de constater ceux qui ont disparu, ceux qui se sont appauvris malgré moi ou ceux qui sont restés intacts. C'est ainsi que j'ai repris goût à écrire sur ce blog.

L'intérêt: ne plus être un simple consommateur, mais avoir une véritable conscience de ce que représente une publication: sa génèse, son message, le média et les liens créés. Ne pas subir l'influence du "like" et mieux formuler ma pensée. Prendre parti, me tromper, rêver. Rendre l'écriture vivante.

J'observe [David](https://larlet.fr/david/blog/2015/oiseau-bleu/) et [Karl](http://www.la-grange.net/2015/01/14/social) fatigués par la course à la publication et militer pour une publication plus réfléchie, auto-hébergée et ne surtout faisant pas la course au clic. A l'opposé, il y a [Ploum](http://ploum.net/series/les-opportunites-manquees-du-libre/) pour qui, le fait de partager prévaut sur le support utilisé, l'essentiel étant de préserver le lien avec l'autre. Dilemme.

## Publier d'abord pour soi
Quand j'ai commencé la rubrique Bouts de code, je voulais avant tout me donner un outil de type wiki, où je pourrais retrouver facilement des lignes de commandes, des outils, des patrons de code qui m'ont sauvé un jour. Eviter de devoir refaire une recherche via un moteur de recherche. Réduire ma consommation du web. C'est sans doute un exercice intéressant pour le cerveau, mais par définition, un bon codeur est faignant. Rechercher parmi ces notes m'a fait gagner en énergie.

Dans les coulisses de ce blog, il y a un README qui m'aide à retrouver mon chemin quand je suis perdu.

## Les passants et les destinataires
Le support, tout comme le format, a également sa part sur l'appréciation de la qualité d'un message.
![I be your Girl](/assets/ego/2015/6/30/i_be_your_girl.jpg)
Si pour l'interessé de ce message, c'est la plus belle déclaration du monde, pour le simple passant, ce n'est qu'un bruit, sans réel intérêt.

C'est le spam généré par l'affluence: l'inévitable déchet des densités trop fortes.


Et comme le souligne [Karl](http://www.la-grange.net), il y a [beaucoup de bruit sur Twitter et la plateforme s'érode](http://www.la-grange.net/2015/02/09/twitter): beaucoup de retweets, peu  de réflexions individuelles. Pourquoi élaborer et formuler sa pensée alors que l'autre a fait ce travail ? Pourquoi chercher à réfûter l'opinion de la masse ? Mon propre compte est ainsi fait et je n'ai pas pris le temps de formuler une opinion propre et personnelle sur bien des sujets.

<blockquote>
Quand on vous demandait de raconter vos vacances, vous les racontiez déjà selon des codes bien précis, qui étaient la syntaxe française, le sens des mots, l'orthographe. C'était votre individualité, mais c'était aussi un passage par des codes. L'individu qui inventerait lui-même son propre mode d'expression ne serait pas compris et parfaitement paranoïaque. Vous passez toujours par un code commun. Qu'il soit la propriété de monsieur Google ou de monsieur Facebook, c'est là un autre problème et l'objet de grandes discussions aujourd'hui. Il y a deux groupes, ceux qui sont pour une possession privée, ceux pour une possession d'État. Ce sont selon moi presque deux Big Brother. Entre l'un et l'autre, cela m'est presque égal. Je préfèrerais des appropriations plus parcellaires.
<cite>Michel Serres : <a href='http://www.lefigaro.fr/secteur/high-tech/2015/03/13/01007-20150313ARTFIG00159-michel-serres-la-question-est-de-savoir-qui-sera-le-depositaire-de-nos-donnees.php'>«La question est de savoir qui sera le dépositaire de nos données»</a> <a href='/categories/cache/posts/la-question-est-de-savoir-qui-sera-le-depositaire-de-nos-donnees'>(cache)</a></cite>
</blockquote>

Les blogs et les forums sont ces parcelles. Diaspora se rapproche également de cet idéal, mêlant possession privée et possession d'êtat. Seulement voilà, c'est le soupçon de sérendipité qui fait l'intérêt des réseaux sociaux: recevoir du contenu aléatoire au gré du temps, des contacts. Twitter est plutôt bon à ce jeu car la sérendipité est créée par le retweet, à condition de suivre des personnes qui varient les sujets de leurs publications.

Suivant la façon dont on parcourt les blogs, cette sérendipité peut être quasi-nulle. J'utilise un agrégateur de flux RSS/Atom où les blogs sont rangés par catégories. Les blogs à thématiques ou d'entreprise ne sortent que rarement de leur thèmes.

Comment puis-je trouver ceux qui ont une plume qui plaît à mon âme ?
Comment créer du lien avec les autres quand on tient un blog, sans aucune notoriété , naturellement et sans pour autant [vendre ses articles comme un dealer de drogue](http://blog.penso.info/2015/03/21/improve-blog-visibility/) ?
Faut-il remettre au goût du jour des annuaires, et peut-être plus communautaires qu'à l'époque? Faudrait-il un [goodreads](http://goodread.com/) des publications de blogs ?
