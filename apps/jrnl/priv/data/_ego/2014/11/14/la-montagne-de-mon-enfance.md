J'ai grandi en face d'une montage.
J'y passais de longues heures à la contempler, l'observer, l'écouter.  
Guetter les cris des [mangé-poulé](http://www.oiseaux.net/oiseaux/petite.buse.html), les chasser du regard, prédire les prochaines rafales de vent, m'émerveiller devant sa chute d'eau dont le débit décuplait après les tempêtes. M'amuser de pouvoir entendre les boeufs au loin. Admirer ses arbres. Me questionner sur leurs âges, centenaires, millénaires?  

Cette montage m'a appris la patience, le goût du détail et surtout l'humilité.