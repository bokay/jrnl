Je n’ai pas quitté Gmail à cause de leur publicité. Non, j'utilise Adblock Plus. Je l’ai quitté par bon sens. Par soucis d'avoir mes courriels conservés par une entité respectueuse de ma vie privée certes, mais aussi par bon sens. 

Aujourd’hui, le stockage des données est si peu cher que Google offre gratuitement quelques 17Go par utilisateur. Je me rappelle du web où avoir une limite de 750Mo d'espace disque était la norme. Maintenant, la limite de stockage est si peu contraignante que l’on devient fainéant et que l’on se met à stocker des choses qui ne devraient pas être conservées. 

## Quel est le but de conserver des infolettres, des messages publicitaires ou des messages invitant à partager un repas à midi ? 

Ces messages seront conservés pendant des années, consommant de l’espace disque et de l'électricité. Outre certains messages à valeur sentimentale ou juridique, tout ne devrait pas être conservé, et c’est du bon sens. La dernière infolettre Amazon en HTML que j’ai reçue aujourd'hui pèse **75Ko**, images non comprises. J’imagine qu’elle a pu être envoyée à au moins quelques 12 millions de français. On peut donc estimer que chaque infolettre envoyée par Amazon pèse 900 Go, soit **près d’1 teraoctet** réparti parmi les boîtes de réception, d’autant que pour Amazon, il suffit d’un achat pour être inscrit de facto à l'infolettre. 

Une solution simple consiste à se désabonner à toutes ces infolettres qui rendent nos boîtes de réception **obèses**. Pour ma part, c’est en migrant ma boîte aux lettres vers un service de courriel indépendant que j’ai réalisé qu’être contraint à peu d’espace disque a aussi des vertues écologiques. Même si je filtrais déjà un bon nombre d’infolettres via des filtres sous Gmail, j'en archivais beaucoup trop. C'est un acte individuel d'[hygiène](https://ploum.net/pourquoi-vous-devriez-viser-inbox-0/).

## Une proposition de protocole

Une autre solution un peu plus sophistiquée serait de revoir le protocole d’email et pourquoi pas imaginer un système de messages et signaux (auto-déstructeurs ou pas).  
Si je souhaite envoyer un message à [Alice](https://en.wikipedia.org/wiki/Alice_and_Bob), ce message sera conservé sur mon compte. J’enverrai donc un signal à Alice lui indiquant qu’elle peut venir sur mon service y lire mon message (manuellement ou par téléchargement). Si mon message ou ma discussion a une valeur sentimentale, juridique ou autre, c’est moi, émetteur, qui décide de la durée de vie du signal.
Pour une bouffe le midi, j’aimerais que le signal ne dure pas plus de 24h. Le reste ne sera que souvenir.
 
Mais il est vrai que d’autres ont essayé de réinventer l’email sans jamais y parvenir...

**Références:**  
[http://www.zdnet.fr/actualites/chiffres-cles-l-e-commerce-en-france-39381111.htm](http://www.zdnet.fr/actualites/chiffres-cles-l-e-commerce-en-france-39381111.htm)  
[http://www.lefigaro.fr/societes/2012/06/01/20005-20120601ARTFIG00645-amazon-poursuit-son-expansion-en-france.php]([http://www.lefigaro.fr/societes/2012/06/01/20005-20120601ARTFIG00645-amazon-poursuit-son-expansion-en-france.php)

