> **L'obsolescence programmée** (parfois aussi appelée « désuétude planifiée ») est le nom donné par abus de langage à l'ensemble des techniques visant à réduire la durée de vie ou d'utilisation d'un produit afin d'en augmenter le taux de remplacement. -[Wikipedia](http://fr.wikipedia.org/wiki/Obsolescence_programm%C3%A9e)-
                                
                                
Je me suis acheté cette semaine un Nexus 5 en remplacement de mon Nexus One. 
                                
                                
Cette bon vieux terminal m'aura duré environ 4 ans, avec une utilisation d'abord très active - la phase découverte - ensuite modérée pour finir en utilisation "vitale" en n'utilisant que les fonctions de base. Dans l'ordre, envoyer des SMS d'abord, puis téléphoner, consulter Google Calendar, Google Maps et en dernier lieu, flâner sur Internet.
                                
                                
Ce terminal aurait pu me durer encore une ou deux années si il n'y avait pas ce problème récurrent à toutes les applications Android: elles grossissent à en devenir obèse.
                                
                                
Si au moment de l'achat, je pouvais sans tricher faire tenir quelques jeux, Twitter, Facebook, Foursquare et d'autres applications, aujourd'hui, j'ai dû sacrifier toutes ces applications phares qui prennent trop d'espace disque.
                                
                                
Triste non ? C'est de notre responsabilité de développeur de veiller à l'empreinte mémoire d'une application. Avec le temps, optimiser n'est pas un *nice-to-have*. 
                                
                
 Malheureusement, nous codons comme nous voyons le monde et non comme nous voudrions qu'il devienne. Derrière ces applications phares, il y a des entreprises, qui pensent fonctionnalités, confort utilisateur en fonction des tendances et terminaux actuels. 
                                
                                
## Alors pourquoi avoir acheté un Nexus 5 et retomber dans cette spirale? 
Et bien tout d'abord, parce que je n'ai pas rencontré d'alternative idéale et non je ne céderai pas au fascisme écologique en achetant un terminal qui ne fait que téléphoner. Mes besoins sont tout autres, le terminal étant une extension de moi, un "meilleur moi".
                                
                                
En attendant que se développent des terminaux comme [Phone Blocks](https://phonebloks.com/en), les développeurs peuvent veiller à coder efficacement en prenant en compte:
        
*   l'empreinte mémoire du package final;
*   l'utilisation des types de variables mieux adaptés à leur contenant pour les langages typés;
*   les accès réseaux;
*   les accès disques et mettre le plus souvent possible l'application en sommeil;
*   les ressources externes.
                                
                                
Comme toutes les métriques business, ce sont autant de métriques techniques à surveiller.
                                
                                
**Recyclons** ! Mon Nexus One deviendra avec une carte Free à 2euros un GPS de moyenne facture et un client bluetooth diffusant du son pour ma voiture. Toutes les autres applications seront supprimées pour économiser de la batterie.
