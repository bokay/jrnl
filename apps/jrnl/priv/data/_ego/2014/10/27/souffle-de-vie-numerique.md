Une fois mort, son corps sera déposé dans un caveau.  
Son corps en décomposition produira des gaz un certain temps.  
Ce gaz, sous sa forme liquide, permettra d’alimenter en électricité le serveur minimal de son alter-ego numérique pour quelques temps encore.  
Une fois ce liquide épuisé, il mourra. Une seconde fois.  
Les restes de son corps alimenteront quant à eux, la Terre. Une fois encore.  
