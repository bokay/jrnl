
<blockquote>
 « Faites-moi rebelle à toute vanité, mais docile à son génie comme le point de l'allongée du bras!  <br>
 Faites-moi commissaire de son sang  <br>
 faites-moi dépositaire de son ressentiment  <br>
 faites de moi un homme de terminaison  <br>
 faites de moi un homme d'initiation  <br>
 faites de moi un homme de recueillement  <br>
 mais faites aussi de moi <b>un homme d'ensemencement</b> »


<cite>Aimé Césaire - Cahier d'un retour au pays natal</cite>
</blockquote>
