Il y a quelques mois, j'ai entrepris d'apporter ma pierre au sauvetage du monde. Une tâche bien hardue! 

C'est en me remémorant l'exercice du poisson d'Ishinagawa lors d'une rétrospective avec mon ancienne équipe, que je me suis laissé convaincre qu'il faudrait chercher la causes des causes pour arriver à des solutions qui ont du sens.

Plus tard, je découvre qu'un certain [Etienne Chouard](https://www.youtube.com/watch?v=oN5tdMSXWV8) est arrivé avec le même processus à la conclusion que la Constitution française nécessiterait une réédition afin de redonner au peuple le pouvoir qu'il a cédé à ses représentants.

Cependant, contrairement à ce monsieur, je ne pense pas que la Constitution soit l'unique cause d'un système qui déraille, mais qu'il y a un gaspillage de temps et d'argent dans des solutions peu efficaces. Trop souvent, nos politiciens veulent soigner un cancer avec des pansements. Sans pour autant en venir à un changement radical de notre république mais plus à une plus grande responsabilisation du peuple, j'ai conçu [FiveCauses](http://www.fivecauses.com/fr/problems) dans l'idée que l'on pourrait améliorer notre quotidien en cherchant à résoudre les causes racines de tout problème.

Ce site est avant tout une proposition, un brouillon numérique, que j'étofferai si l'idée convainc.
