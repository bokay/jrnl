<figure>
<img src='/assets/ego/2017/5/22/appletree.jpg' alt="The apple tree and the old ladies, Ottawa, ON" title="The apple tree and the old ladies, Ottawa, ON"/>
<figcaption> Ottawa, May 22nd 2017 </figcaption>
</figure>

I've been wondering what was inside those two old japanese ladies mind. Did those apple trees remind them of Sakura's cherry blossoms? Were they nostalgic or just happy to feel their youth again? Or maybe were they simply contemplating true form of beauty. I'll never know.

<a href="https://www.instagram.com/p/BUaFqWKgpbz/"> Instagram</a>
