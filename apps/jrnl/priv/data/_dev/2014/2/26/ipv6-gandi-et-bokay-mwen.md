Afin de regagner mon droit à [une vie numérique privée et protégée](https://www.laquadrature.net/fr/Vie_privee), j’ai récemment fait l’acquisition d’un nom de domaine “bokay.io” et la location d’un espace VPS.

Pourquoi Bokay.io ?
-------------------
  *Bokay* en créole signifie « *chez* » et donc l’expression *bokay yo* signifie « *chez eux* ». Ce nom est propice à un nombre insoupçonné de jeux de mots. Par exemple, `kriye.bokay.io` signifie « *appelle chez eux* ». C'est un beau mélange de créolité et de technologie avec le TLD en .io. Un nom de domaine qui évoque bien ce que je suis, créole et développeur.

  L'idée est d'avoir un serveur centralisant les données personnelles importantes dont:

  * **Un webmail**

      Le webmail roundcube de Gandi ne répond pas à tous mes besoins notamment il lui manque les filtres automatiques, condition sine qua none pour moi pour quitter Google Gmail en toute quiétude.

  * **Un outil de stockage des données personnelles**

      Je suis un friand utilisateur de Google Drive. 80% de mes documents papiers sont scannés. Je compte remplacer cet outil par [Bittorent Sync](http://www.bittorrent.com/sync).

      Bittorrent sync me permettra de créer un réseau P2P de points de stockages: un raspberry pi, le VPS et chaque ordinateur de mon domicile. Son seul défaut: Il n'est pas libre et donc difficile de juger du respect de la vie privée.
      De plus, je compte y héberger une série de services web basiques pour y stocker mes notes (en remplacement d'Evernote).

  * **Un outil de developpement**

      Je veux héberger mes gists et mon code source personnel, être maître de l'infrastructure et réduire mes coûts (heroku, github). Gitlab semble être une bonne alternative pour du code que je ne partage pas ou peu, et ensuite, si possible, docker pour le déploiement d'applications.

      J'aimerais y stocker mes stacktraces également. C'est l'usage que je fais actuellement d'Evernote en plus de mes notes personnelles.

Pourquoi un VPS chez Gandi?
---------------------------
  * Un VPS est [configurable à tout niveau](https://www.gandi.net/hebergement/serveur/achat) et au moment choisi;
  * Tu payes les ressources que tu utilises;
  * Un VPS est mutualisé, partage des ressources disponibles;
  *  *NoBullshit* est leur tagline !

 A l'heure de l'abondance, il est bon de prendre conscience que nous utilisons trop de ressources et qu'il est bon de les optimiser.

  Je souhaite également expérimenter les [Linux containers](http://linuxcontainers.org) et pouvoir isoler les différents services.

IPv6
----
  Gandi propose de réduire le coût du VPS si il est loué avec une interface ipv6 au lieu d'une ipv4. Autant dire que c'était l'occasion révée pour moi de faire joujou avec l'ipv6.

  Un petit cheatsheet:

  -  Ping
     Utiliser `ping6` au lieu de ping. D'une manière générale, les outils réseaux pour ipv4 sont suivis d'un 6 à la fin du nom.
  - **SSH**
    Utiliser `ssh` avec l'option `-6`:
    Exemple: `ssh -6 toto@2001:4b98:dd0:51:216:3eff:feb6:5267`
  - **Une adresse ipv6 dans un browser avec un port**
    Entourer l'adresse ip de crochets.
    Exemple:          `http://[2001:4b98:dd0:51:216:3eff:feb6:5267]:8080` pour le port 8080
  - **Configurer le DNS de Gandi**
  Utiliser une entrée AAAA au lieu de A
       Exemple: `@ 10800 IN AAAA 2001:4b98:dd0:51:216:3eff:feb6:5267`
