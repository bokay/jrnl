The web is nothing more than lists and tools to manipulate those lists.

* A blog: a list of articles
* Facebook: a list of many things(including personal stories, videos, photos)
* Twitter: a list of short stories
* Amazon/Any online shop: a list of things to buy
* Feedly: a list of pointers to blogposts
* *Lists and sets everywhere..*

If you  do the exercice of removing their CSS, then you'll see that, hidden among lots of make up, there's just lists of things (videos, short texts, sounds, books, ...).

### What makes a list valuable?
Obviously it's content, but not only. The interaction people have with those list is also determinant.

What we know about lists:

* we can insert items
* we can delete items
* a list can contain other lists
* there are many kind of lists (ordered, unordered)

### Why Are we that limited ?
I've made the exercise to examine my own house. Naturally, my house is a set of pieces. Each pieces has its context, its list of things. For instance, in the kitchen, I have a p
lace dedicated to all the table set.

Lists are definitely in every places exposed by mankind, especially in occidental culture. Lists are pleasant to our mind.

After all, aren't we all nothing but a well organized set of amino-acids.
