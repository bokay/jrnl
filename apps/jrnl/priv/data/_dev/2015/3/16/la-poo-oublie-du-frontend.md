Depuis le mois de janvier, j'ai pu m'adonner aux joies d'un framework JS. J'avais choisi EmberJS pour la stabilité du projet, sa documentation, le faible nombre d'issues ouvertes sur Github et parce que son créateur comprend très bien les problématiques liées à l'utilisation de rails.

Après avoir essuyé quelques gouttes de sueurs, bâti de belle choses, je décide maintenant de m'en passer. Ce framework a la prétention de satisfaire 80% des projets web. Pour les 20% restants, il faut savoir s'armer de courage pour arriver au résultat escompté. La première critique que j'ai eue est que je me mettrais à recréer la roue, et que j'en viendrais à créer mon propre framework. Ce n'est pas impossible, mais mes motivations sont toutes autres.

Avez-vous déjà mis le nez dans du code de développeur front-end? C'est le souc !
Il y a un style évident de programmation que l'on peut qualifier de procédural. Une suite d'instructions, parfois au milieu du DOM dans les pires cas, pouvant atteindre des longueurs inimaginable. C'est d'ailleurs pour cela que je n'aime pas le terme "développeur front-end" car il s'agit plus d'intégration.

Aujourd'hui, j'ai eu l'occasion d'essayer d'appliquer quelques notions que j'estime correctes de développement à du code front-end. Un découpage par object, des classes disposant d'interfaces publiques, des méthodes ne dépassant pas 5 lignes. Le résultat est là. Il est tout à fait possible d'avoir des objets réutilisables et lisibles, n'utilisant que le couple Coffeescript/Jquery. Il est ainsi possible de se passer de framework JS, certes perdre en fonctionnalités de base, mais d'avoir un code concentré sur l'essentiel.

```coffeescript
  class APP.TributeeDropdown
    constructor: (module, tributees)->
      @module = module
      @placeholder = module.find('.template_placeholder')
      @tributees = tributees

    build_list_options: =>
      @add_select_to_dom()
      @prompt_default_tributee()
      @setup_select2()

    add_select_to_dom: =>
      output = "<input class='js-tributees' type='text'/>"
      @placeholder.html(output)

    setup_select2: =>
      $('input.js-tributees').select2
        data: @select_data(),
        multiple: false,
        createSearchChoice:(term, data) ->
          new_term = term + '(new)'
          {id: term, text: new_term}

    prompt_default_tributee: ->
      default_name = @tributees[0].name
      @module.find('.js-tributees .select2-chosen').html default_name

    select_data: =>
      select_data =[]
      $.each @tributees, (key, val) ->
        select_data.push {id: key, text: val.fullname}
      select_data
```

Ce que je peux reprocher de prime abord à cet exemple, c'est l'approche à la React visant à mélanger élément du DOM à du code Javascript. L'idéal serait d'avoir un outil de templating simple, peut-être [jquery-template](https://github.com/codepb/jquery-template). L'exploration continue.
