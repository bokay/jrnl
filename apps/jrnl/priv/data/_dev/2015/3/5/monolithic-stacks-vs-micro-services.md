![Monolithic](/assets/dev/2015/3/5/monolithic.jpg)

We use, in my company, a gem called Spree offering an e-commerce platform for free. The project itself is now composed in several subsections: a core, a basic front-end, an API and an admin back-end.

For each of our project, we run a full ruby stack with the spree gem, a front-end in client’s colours and flavour and glue for specific requests and features. Each stack has his own server.

Spree has known major releases, implementing a whole different structure from 1.3 to 2. The drawbacks of such architecture are obvious: the cost to upgrade and the maintainability. In terms of happiness, who enjoys to endure hard migrations from old stack, technology and also way of coding. If it remains an interesting exercise to learn what we used to do, and understand why nowadays practices are better, it remains time and energy consuming.

Some times ago, a very different system existed using SOAP, XML contracts and messages. We can consider that REST API using JSON messages and pure HTML contracts are somehow the mature version of this old protocol.

Yet, we seemed to have given up the idea of distributed services. With the rise of javascript frameworks and a clear separation between the back-end and the front-end, this idea of running services doing one job will get more and more popular.

If moving right away to a fully decoupled organization is definitely a bad idea for its cost reasons, we can certainly try to [extract services one by one](https://sourcegraph.com/blog/live/gopherconindia/112656568167) and realize some day that we run a fully decoupled system. If the power of decentralized system is already proven, it remains the most difficult solution, but we can see it as a seek of [sustainability](https://en.wikipedia.org/wiki/Sustainability).

Your move.

---
Further readings :  
[http://martinfowler.com/articles/microservices.html](http://martinfowler.com/articles/microservices.html)
[https://sourcegraph.com/blog/live/gopherconindia/112656568167](https://sourcegraph.com/blog/live/gopherconindia/112656568167)  
[http://devops.com/features/benefits-micro-services/](http://devops.com/features/benefits-micro-services/)  

Photo credit: [Vonderauvisuals](https://www.flickr.com/photos/vonderauvisuals/8906507107)
