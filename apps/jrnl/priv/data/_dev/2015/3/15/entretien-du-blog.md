A chaque parution d'article sur ce blog, j'essaye d'apporter une amélioration, même minime, à cet espace. C'est pour moi l'occasion d'expérimenter discrètement, me faire un avis sur certaines architectures et m'amuser.  En cette fin de semaine cependant, je me suis attelé à transformer quelque peu la mécanique ce blog.

## Supprimer la base de données

En lieu et place de mongodb, j'utilise désormais le système de fichier pour stocker mes articles. A l'instar de [jekyll](http://jekyllrb.com/docs/frontmatter/), mais sans pour autant générer tout le site en statique, j'utilise un fichier markdown couplé à un fichier YAML pour stocker les méta-données comme le titre, la position géographique et les étiquettes
. Je gagne en lignes de code et donc en complexité, en souplesse au niveau des déploiements et j'élimine en passant un *point individuel de défaillance*.

```bash
➜  ls posts/_ego/2015/2/22/*
posts/_ego/2015/2/22/de-la-difficulte-detre.yml
posts/_ego/2015/2/22/de-la-difficulte-detre.md
```

## Supprimer les composants javascripts

  Une aberration de la première page était le chargement de polices et javascript totalement inutile pour le visiteur, mais nécessaire à une partie d'administration agréable uniquement pour moi. En troquant la base de donnée pour de simples fichiers, je me suis libéré de tout ça. La ligne de commande est mon amie.

## Ajout d'archives

Après quelques échanges avec [David](https://larlet.fr/david/stream/2015/01/05/) à ce sujet, j'ai ajouté la gem [ruby-readability](https://github.com/cantino/ruby-readability) et je peux désormais archiver le web pour assurer la perennité des liens des articles.


En terme de performance, je passe de [1.256s](http://www.webpagetest.org/result/150315_BB_KX2/) à [0.577s](http://www.webpagetest.org/result/150315_QX_MNV/) sur la première page, pour quelqu'un habitant en Islande et avec une connexion câblée. Le test aurait été bien plus pertinent avec une connexion moins fiable cela dit.

Par la suite, j'aimerais:

- introduire les dates dans les URL. Le défi sera de ne pas casser les liens existants
- éliminer la dépendance avec Amazon S3 pour l'hébergement des images. Le fait de savoir que ces données soient potentiellement [alimentées électriquement par du charbon](http://www.liberation.fr/terre/2014/04/10/twitter-et-amazon-parmi-les-sites-les-plus-polluants-du-web_994597)[(cache)](/categories/cache/posts/twitter-et-amazon-parmi-les-sites-les-plus-polluants-du-web) me gène.
- automatiser mes déploiements Docker via les git-hooks
- gérer la pagination
- passer de Rails à Sinatra, plus léger et adapté à l'usage que j'ai maintenant de ce blog
- engager une réflexion sur le partage, la création de liens numériques ou humains et aborder la possibilité de me passer de plateformes tierces, dont Twitter et Facebook.
