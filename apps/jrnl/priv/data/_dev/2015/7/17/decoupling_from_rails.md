After an exchange with Avdi Grimm trying to understand why he does not like [Service Objects](https://twitter.com/avdi/status/613833755195756544), I also put this video here, as it means a lot to me.

<p><iframe width="560" height="315" src="https://www.youtube.com/embed/tg5RFeSfBM4" frameborder="0" allowfullscreen></iframe></p>

I've literally changed my way of coding after seeing this. I haven't grabbed all the concept at once, but after some time exploring this technique, it all makes sense. From the repository, the interactors, how to test them so they can be as fast as possible.  

I can see that it has also influenced the work of Nick Sutterer with [trailblazer](https://github.com/apotonick/trailblazer).

The repository pattern is great as it also gives you the possibility to use something else than ActiveRecords. Sequel for instance. That way, you won't need to wrap and delegate like he does in his video, but you'll have straight PORO models. That's the path taken by the [lotusRB framework](http://lotusrb.org/).  

Also, I'm glad because it brings up to the scene a way of coding that I've seen only in Java frameworks. Rails was great, straight-forward. But with a large codebase, maintenance became a nightmare. It's like it had set aside all the experience of Java accumulated over the years and unfortunately missed the point.  

Thank you Jim.