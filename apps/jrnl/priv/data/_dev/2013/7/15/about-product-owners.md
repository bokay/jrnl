
What makes a good product owner?
Many people improvise themselves PO without knowing what's behind that role. In this article, I will intend to define what developers expect from a product owner and what I've learnt from my own experience, being a developer in front of a product owner.


## Set the vision

The PO comes with a vision. Not something for short-term like "I'm going to build a calendar for blind people". No. A vision is more like "I want to help blind people to be better organized". The difference is subtile but quite huge. The last sentence is more suitable to build anything that fit a need.

With this vision, we could build a calendar, a time-track manager or whatever could help blind people being better organized.

## Find new hypothesis to validate

The PO has the hardest and greatest duty of finding hypothesis to validate. Those hypothesis will lead the project all along.

Let's take an example.

According to the vision, it would be interesting to build something that will help blind people to be better organized. So what about building first a calendar? Is an audible calendar a better fit? Would a physical Braille system be better?

Well, to validate that point, maybe the answer will come from your future users. So let's ask a set of blind people what they think about it. Are they willing to pay for that feature? How much ? Do they already have something that fulfill that need ? Do they care about it?

All those questions will lead the product owner to conclude if yes or not, it's a good idea.
If the data gathered during the analysis say no, don't do it. It may lead to a waste of time, money and energy. Pivot your hypothesis to understand what can bring real value to the user

## Challenge the hypothesis permanently (every 2 weeks)

Every feature a PO propose to its dev team should have been validated before. 

Many developers write a test before writing the code. Writing code this way lead to code with great quality. That's the same with product value. Write tests, set your metrics before asking your dev team to implement any feature.

## You're gonna be pushed around

Getting into a Lean cycle is tough. Your ideas are going to be challenged everytime. It's not only a matter of feature to be implemented. No, getting the lean boat is also a human journey. You're going to be challenged. So will be the business model you proposed, your ideas and you. Trust in you to defend your point of view when necessary and accept other ideas if there's a better way to achieve the goal. Accept different point of views. Learn from your team.

## Don't be a manager, trust in your fellows

Yes, and even if you've got the right to force your solution.
For instance, you'd like to increase the conversion. Your first take would certainly be to put a red the button or make it bigger. Chances are, it's the good solution. But if you have a designer, he will suggest you to rearrange the display and make important content more visible. Developers will tell you to do a one-click subscription maybe.

What to do ? The response is in the middle of all what your team suggest you. Accept to do differently, challenge your ideas.

## Don't be the idea guy

If you come to a situation where there are "those who do" and "those who think", you're totally wrong. Don't think that those who do are not able to think. There's no place for the guy who come up with an idea and does nothing in return. You're a product owner? Well, you should definitely take part of the product: design, code, wording, anything that gives real value to the product. There's enough to do when bootstrapping a product.

The 37Signal's configuration is quite optimal as the boss, Jason Fried, is a designer and he really takes part of the product's life.

Take a look at what [DHH says about it](http://37signals.com/svn/posts/2188-theres-no-room-for-the-idea-guy)

## Focus, do like if it was your last sprint

Time is infinite, but your life is limited. So is a project lifetime. If you expect it to last 3 years, maybe it will last only 6 months. Don't pretend to know the future. Act like if every sprint was the last one. Don't waste time and money by doing things that doesn't bring value to the final user. Good feedback fuels a team with positive energy.

## Think forward

Don't build a solution for the next two weeks. Your dev team expects perennial solutions, not solutions to maintain every week. The more you ask perennial solutions, the more you will have time and resources to validate new hypothesis.
