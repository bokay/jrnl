<img src="http://farm9.staticflickr.com/8242/8500035309_6e96878170_n.jpg">

This week, I've experienced the lead of a retrospective.  
I used the book [Agile retrospective: makes good teams great](http://www.amazon.fr/Agile-Retrospective-Making-Teams-Great/dp/0977616649) to choose my exercises.


Basically, the retrospective is  divided in 5 steps:

*   Checkin
*   Gathering data
*   Generate insights
*   Deciding what to do
*   Closing  

Checkin
-------------
It was important to me to have everybody relaxed after the hard week we had. So, after presenting the main goal of the retrospective and the agenda (timing), I've chosen to introduce the satisfaction poll. I wanted a clear answer on a question I was asking myself and according to me, was a problem for the team, and see if, indeed, the feeling I had was shared.

This poll had only 3 items to force the team members to make a clear choice and avoid status quo *BUT* with funny expressions. A good balance between fun and focus.

 Gathering data
-------------
The answer of the checkin was crucial as it could have changed all I planned. 

If most people were satisfied, then I would certainly choose a Learning Matrix. But they wasn't so, I've chosen to lead them to the Ishikawa diagram also known as fishbone to find the root cause of the problem I expressed during the checkin.

As a debrief, the team members had to choose the most relevant causes they would like to investigate.

Generate insights
-------------
If the Ishikawa diagram was about the **"Why?"**, I wanted now something about the **"How"**.

So, I've chosen a simple brainstorm/filtering activity. People simply writing ideas on sticky note and sticking them on a white board.

Filtering was done together as a team. A simple dot-vote would have helped instead.

Deciding what to do
-------------
Time was short, and unfortunately, that was the part I less prepared. 

So we wrote full sentences of what we have decided to do on a flipchart sheet, visible for everybody and available for missing teammates. 

Closing
-------------
Forming a circle, everyone gave one by one their feelings, hopes and fears for the bright next iteration.

Personal conclusions
-------------
I liked it a lot and my personal action is to facilitate another retrospective.

It sometimes takes guts to stay gentle and invite people to avoid off-topic and sometimes, it's good to let the people express themselves, debate and have at the end maybe a meaningful tip on how to continue the retrospective.

The combo Ishikawa+fishbone+brainstorm was the perfect set of exercises for our case and for what I had in mind, and also, it offered the choice to the team members to escape themselves from what I first planned. I don't know yet if the actions taken are the right actions but at least, we had a common engagement for the next iteration.


Sources
-------------
 *[Agile retrospectives, make good teams great](http://www.amazon.fr/Agile-Retrospective-Making-Teams-Great/dp/0977616649)*  
*[Credit photo](http://www.obiwi.fr/la-vie-d-obiwi/obiwisteries/85043-obiwish-2010-pour-que-culture-rime-avec-developpement-durable)*