Very often, I see people talking about integration tests instead of acceptance tests and vice-versa.

When using Capybara for testing, you're mostly doing acceptance but not integration, or not directly.
Acceptance is what you test from a user point of view.
Integration tests however defines the interactions between objects.

When coding alone on personal projects, I find acceptance tests useless and I don't think I will someday put any cucumber in my Gemfiles. Too much pain when integration tests do the job.

For that reason, I use rspec both for unit testing and integration testing. 