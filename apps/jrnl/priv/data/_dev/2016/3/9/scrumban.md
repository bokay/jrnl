## Introduction
  I had the chance in the past to experiment with all sort of process, from dictatorship to full agility. <br />
  I've seen the waterfall model in a maintenance pole of a service company, dictatorship in startups, agility in two intra-entreprises (a startup pole living within a bigger company, but with its own rules), one with scrum, the other with scrumban. I'll discuss the latter, as they were the most efficients to deliver constantly quality over time. <br />

## Scrumban
<figure> <img src='/assets/dev/2016/3/9/scrumban.png' alt="Scrumban"/> <figcaption>Scrumban </figcaption> </figure>

### The stage
   As a team, we chose kanban over Scrum because we wanted to experiment it and we would fallback on pure scrum if the experiment was a failure. <br />
   We were a very small team of 5 people, working altogether, in the same room:

  - 2 backend developers (a veteran and a junior, me)
  - 1 frontender
  - 1 community manager
  - 1 product owner
  We were mostly beginners at what we were doing. That was my first rails project, our frontender was coming from the print world, first time also for the community manager and certainly first intrastartup for our product owner.

### The design
We iterated several times to finally find the kanban that suited us best. Very personal and specific to the composition of our team. The kanban we end up following was composed of 5 columns for each part of the life of a feature:

- UX
- Frontend
- Coding
- Wording
- Testing by PM

And 3 rows :

- 2 for feature stories
- 1 for bugs/chore

Aside from that, we had the playground of our PM : the backlog. <br />
She was prioritizing the next things to be built in this section, based on the feedback she was gathering through past iterations. What's the next emergency to be built, what's the most important issue to be fixed ?

We valued short feedback loop.

### The rules
Discipline is the keyword here and we had several simple rules instored :

   - One card per row
   - One card per columns <br/>
     The pace was important to provide good quality software. This allowed us to focus on one thing at the time.
   - A card goes from right to left, and never jumps a column. <br/>
     This simple rule forced everyone to take part of the quality of a feature.
   - A card can go back to be reajusted
   - Each feature starting with the UX is the affaire of everyone. No one is the idea guy in the room. Ideas come from everyone. That's why we discussed the UX of our features around a paperboard, having everybody drawing. Until we got a UX making consensus. However, how it would look like was solely upon the frontend designer opinion, who then could challenge his view with real user exercices. He did user polls video taped, got feedback with tools like crazyeggs, challenged his ideas with other designers.

In a pure scrum approach, dealing with the debt is done when establishing the contract on what will be delivered at end of sprint. Dealing with the debt is a matter of hygiene. It's something that is part of the project life, not a chunk of code that has to be delivered every 6 months.

### Stand ups
Even though we had a very transparent kanban, we also did standup meetings, all gathered around the kanban to talk about how we could do the work smoothier and having everybody knowing where something blocks so we could help. One is blocked, everybody gets blocked. Thus, just like in Toyota chains, it's the affair of everyone to help solving the issue.

### The technical details
Heroku played a huge part in our setup because it allowed us to create environments very easily (on the fly and for free) for each feature we wanted to develop. Each feature had it's own environment and then we would merge it eventually into the Big Master Branch, and deploy it. Once over, delete. Start over. <br />
We were doing continuous deployment (many deploys per day, depending on velocity) and continuous integration with Jenkins (phew!) <br />
We also did pair programming all the way, may it be between developpers or a bit less often, between frontender and developper. As we were a small team, we didn't have styleguides. It was all about communication and transparency.

### Day off
We took a day for ourselves. That day would split into three activities.

   1 - Retrospectives <br />
   Basically, it was the time for us to gather as a team and talk about what went wrong during the past week and how we could improve ourselves. The result after this session was actions to try on next week. It may be over communication, our workflow, our we deal with situations with the client, etc. <br />
   It was the time to grow up as a team from team member feedbacks. Just like Yoga, we entered the session giving up what we had on our shoulders, what bothered us, shared our fears but also celebrated our successes. A weekly human feedback loop. #continuousImprovement

   2 - Choose what comes next <br />
   This is the major diversion from Kanban where everything is in a "just-in-time" flow, meaning that the flow is not broken at the end of the week. Here we decided to use the scrum week iteration for several reasons. One was because the smallest tasks was always picked because it was supposedly fast to implement, but was not necessarily of value for the end-user and for the project.

   3 - Experiment <br />
   The whole afternoon was dedicated to experiment stuff, play with new technologies and share with other teams.


## Conclusion
  We did many things bad for sure, but I'm intimately convinced that we succeeded delivering quality over time. <br />
  Out of this experiment, I'm also conviced that a frontend developper is more qualified to drive a project than a backend developper, because his decisions are those who impact most the end-user. He's definitely the key of the success of a project if the end-user is the one giving the money. <br />
  It was not kanban or scrum by the book, but we built our own tool, for our very own team.
