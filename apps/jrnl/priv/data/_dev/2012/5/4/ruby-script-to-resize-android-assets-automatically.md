As I said in my previous article, I’ve been introduced to Android development for my last mission. I had a lot of assets to resize from HDPI to MDPI and LDPI.

At the begining, since I didn’t have a lot to resize, I did it manually and I figured out that it was worth a simple script to make life easier.

This is what I did in Ruby and the RMagick gem, a wapper to the classic Image Magick. 

```ruby
#!/usr/bin/env ruby -wKU
require'RMagick'
includeMagick

Dir.foreach("drawable-hdpi") {|x|
  if/png/.match(x)||/jpg/.match(x) then
   myPic =Image.read("drawable-hdpi/"+x.to_s).first
   currentColumn = myPic.columns
   currentRows = myPic.rows

   mdpiColumns = currentColumn*0.67
   mdpiRows = currentRows*0.67

   ldpiColumns = currentColumn*0.5
   ldpiRows = currentRows*0.5

   #TRAITEMENT MDPI
   myPic.change_geometry(mdpiColumns.to_s+'x'+mdpiRows.to_s) { |cols, rows, img|
     img2 = img.resize(mdpiColumns, mdpiRows)
     img2.write('png:drawable-mdpi/'+x)
   }

   #TRAITEMENT LDPI
   myPic.change_geometry(ldpiColumns.to_s+'x'+ldpiRows.to_s) { |cols, rows, img|
     img2 = img.resize(ldpiColumns, ldpiRows)
     img2.write('png:drawable-ldpi/'+x)
   }
  end
}
```


You can fork and improve yourself the original code from [my github repository](https://github.com/nicocharlery/android-assets-resizer)