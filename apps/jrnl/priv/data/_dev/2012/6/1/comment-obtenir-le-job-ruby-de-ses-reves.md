Je vais décrire ici les différents moyens que j’ai utilisé pour arriver à mes fins. Je pense que ces solutions peuvent également s’appliquer à la prospection de clients en tant que freelance.


## Meetup
Consulter les meetups autour de la techno qui vous intéresse. Dans mon cas, Ruby, Ruby on Rails mais aussi l’écosystème de ces technos (MongoDB, Heroku,...) et regarder les entreprises partenaires de l’événement.
Si vous y participez, il y a de fortes chances que vous trouviez au meetup un employé de l’entreprise partenaire et entrer en contact avec celui-ci.
De plus, il peut être intéressant de regarder également les partenaires des événements précédents. Ce ne sont pas forcément les mêmes, mais ils peuvent peut-être proposer des postes aujourd’hui

## Remix jobs, Human Coders, Rails France, etc.
Il existe maintenant des sites qui se sont placés dans la niche des technos de startup comme JobProd, Human Coders, Remix Jobs ou encore des plateformes dédiées à un framework à l'instar de Rails France.
En ce qui me concerne, j’ai centralisé les flux RSS de ces plateformes dans un dossier de Google Reader pour avoir une meilleure visibilité et perdre moins de temps. Certains sites comme Human Coders ont le mérite d’offrir un RSS par techno, ce qui donne un résultat moins noyé dans du bruit, le bruit étant ici des offres d'emploi totalement hors-propos.

## Google
On n’y pense pas forcément tellement ça peut paraître bête et simplet mais une recherche Google basique avec des mots-clés comme "job dev rails paris" peuvent amener de nouvelles pistes auxquelles on ne pense pas forcément de prime abord.

## Linkedin
Rechercher "ruby" dans son réseau et regarder les entreprises où les gens sont en postes.  
Pas besoin de consulter le profil entièrement pour avoir cette donnée.

## Twitter
Idem que Linkedin. En cherchant les gens qui tweetent en français autour du Ruby et en regardant leur blog, on arrive de fil en aiguille aux sociétés qui les emploient.

## Les cabinets de recrutement
Je suis moins fan mais ça peut être une solution. Ils ont accès à plus d’entreprises mais il faut garder à l’esprit qu’ils ne sont pas gratuits et que la marge qu’ils se prennent se répercute forcément sur l’offre de salaire finale.

## Les entretiens
J’ai pris pour habitude depuis mon expérience aux US de toujours (ou presque) envoyer un mail de remerciement à la suite d’un entretien. En effet, ça aide à garder le contact mais aussi ça permet d’avoir des retours parfois inattendus, sur par exemple les points à améliorer techniquement, les malaises durant un entretien et parfois même, obtenir des contacts d’autres entreprises. Les entrepreneurs d’aujourd’hui se connaissent et se côtoient.

Si vous avez d'autres façons de faire ou astuces, n'hésitez pas à partager en commentaire !

