As a freelance, my last mission has been a treat with lessons learnt.

  I’ve made my first steps on Android development by doing from scratch an app that can hit the thousand concurrent sessions, learnt how to architect such a structure and most of all, learnt something that can’t be taught on books: a **failing upgrade**.
            
  We are one month ago, birds are singing, springs is coming and our new release is ready. The challenge was to have a successful upgrade for thousands of users. Tests say everything’s alright, beta-testers give us the last feedback and errors to correct and we’re good to go.
            
  Unfortunately, things didn’t happen as expected as the upgrade failed for a large amount of users. Not all of them, but a significant part. Enough to let our stress climbing from a lethargic state to a “WTF is goin’on” state.
            
  We’ve pointed out the problem, did the fixes but we did something very very bad according to me: launching the mass-communication too early. Indeed, the news was everywhere, getting a lot of users. New users didn’t have any problem but old users had some local database troubles.
            
  As it was something hard to reproduce with our few testing mobiles,the correct strategy would have been to first launch the app on Google Play, second observes the errors that we could monitor for a short period like 2 or three hours, and THEN take a decision: either remove it quickly from the Google Play, or launch the successful mass-communication to newspapers, blogs and so on...
            
  Thanks to **Flurry** and the **Google Play’**s new error feedback platform, we have been able to be alerted of what happened. So, I’ve learnt some critical things. First, the users reaction is something quite viral and exponential when they’re not happy. Google Play doesn’t let the owner to sort the comments to let visible only the most grateful. Upgrades are definitely critical stages that need strategies. And to finish, it’s important to have real-time error feedbacks on a mobile app in order to react fast.