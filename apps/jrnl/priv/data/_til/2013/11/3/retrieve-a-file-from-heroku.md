On my current project, I had to face a not-common situation. A developer on the team who had a repository on Github and the project used it. But one day, that developer left the project and simply removed that important part of the project. 
Fortunately, the project is hosted on Heroku and since Heroku use EC2 instances, it was easy to find a copy of the missing gem.
                
The key was this command to get a shell on the EC2 instance:

`heroku run bash -a your_app`
                
And then, to simply use scp (secure copy) to retrieve the directory:

`scp -r heroku_directory  user@your_ssh_server:path/to/download`
                
And use 'find' command to find out where is the file you're looking for.
                
Voilà !