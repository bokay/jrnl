Here is the Dockerfile that I use for this blog:

```bash
# Format: FROM    repository[:version]
FROM       ubuntu:latest

# Format: MAINTAINER Name <email@addr.ess>
MAINTAINER 'http://niko.bokay.io'

RUN echo 'Reinstall app ------'
RUN apt-get update

## Default Packages
RUN apt-get install -y -q ruby1.9.1 ruby1.9.1-dev build-essential
RUN apt-get install -y nano wget links curl rsync bc git git-core apt-transport-https libxml2 libxml2-dev libcurl4-openssl-dev openssl sqlite3 libsqlite3-dev
RUN apt-get install -y gawk libreadline6-dev libyaml-dev autoconf libgdbm-dev libncurses5-dev automake libtool bison libffi-dev

# install RVM, Ruby, and Bundler
RUN /bin/bash -c -l "gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3"

RUN \curl -sSL https://get.rvm.io | bash -s stable

RUN /bin/bash -l -c "rvm requirements"
RUN /bin/bash -l -c "rvm install 2.1.5"
RUN /bin/bash -l -c "gem install bundler --no-ri --no-rdoc"
RUN /bin/bash -l -c 'rvm use 2.1.5 --default'

# Install Pygments
RUN apt-get install -y python-pip python-dev build-essential
RUN /bin/bash -c -l 'pip install Pygments --upgrade'

#Bundler
RUN /bin/bash -c -l 'gem install bundler'

RUN free -m
RUN /bin/bash -l -c "gem install nokogiri -v '1.6.6.2'"
RUN free -m

# Install the blog
WORKDIR /app
RUN /bin/bash -c -l 'git config --global user.email "nicolas@bokay.io"'
RUN /bin/bash -c -l 'git config --global user.name "niko-serveur"'

RUN echo 'Reloading app --------'
RUN /bin/bash -l -c "git clone https://<PRIVATE_GITHUB_KEY>:x-oauth-basic@github.com/nicocharlery/carnet.git /app"
RUN /bin/bash -c -l 'bundle install --without development test'

EXPOSE 3030
CMD /bin/bash -c -l 'git pull -f && bundle exec rackup --host 0.0.0.0 -p 3030'
```

# Usage:
- `sudo docker build --tag bokay/blog .`
- `sudo docker run -d -P --name carnet_web -p 3000:3030 bokay/blog `
- `sudo docker start carnet_web `
- `sudo docker logs carnet_web `
- `sudo docker stop carnet_web `

This blog rests upon Sinatra/Rack and does not use any database.
To deploy a new article, I just have to push the static files on the git server and then stop/start the container.  

Also, if I want to reinstall partially or completely, I play with the docker cache by modifying the `RUN echo` lines. Basically, I add a dash or remove one. Then, I rebuild. It happens when I add a new library to the codebase.
