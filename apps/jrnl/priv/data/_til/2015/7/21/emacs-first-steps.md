I'm a former VIm user.

I've used it since 2 years ago and I feel quite happy with it.
I write code mostly in Ruby and I often go deep in the code in order to know what's behind the hood.
I know I can make a good use of ctags but I'm also interested to know how the former competitor handle that task. I've already been amazed to see the integration of the `grep` command in `dired`. I want to go deeper now.

Like [Sacha Sua](http://sachachua.com/blog) says, let's be **"ambitextrous"** !

#Basic knowledge

There is that very valuable resource that gives you the corresponding VIM keystrokes for Emacs: [http://hyperpolyglot.org/text-mode-editors](http://hyperpolyglot.org/text-mode-editors)

## Navigation
- `C-e`: go to end of line
- `C-a`: goto begining of line
- `M-<`: goto begining of file
- `M->`: goto end of file
- `M-{`: previous paragraph
- `M-}`: next paragraph

## Text manipulation
- `C-v`: Page down
- `M-v`: Page up
- `C-d`: Delete character under cursor
- `M-d`: delete next word

## Text navigation
- `C-f`: forward, next character
- `C-b`: backward, previous character
- `C-n`: next line
- `C-p`: previous line
- `M-f`: next word
- `M-b`: previous word

## Window navigation
- `C-x o`: Switch window
- `C-x 3`: Split horizontally
- `C-x 2`: split vertivally
- `C-x 1`: quit every window but current one
- `C-x b`: switch buffer
- `C-x k`: kill buffer

## init.el reinitialization
Open your `init.el` file and `M-x eval-buffer`

## French accents
My setup is a Qwerty keyboard + OSX and I'm French. I need to toggle an input method in order to make the accents working.

- `C-x RET C-\`: set-input-method to `latin-1-postfix`
- `C-\`: toggle the input-method
- compose letters in diacritric mode (eg: `e'` to have a `é`)


- `C-x RET C-\`: set-input-method to `latin-1-postfix`
- `C-\`: toggle the input-method
- compose letters in diacritric mode (eg: `e'` to have a `é`)

I also could choose to [setup the mac modifiers](http://www.zebulon.fr/questions-reponses/anti-slash-sous-mac-os-x-191920.html?page=3), but I want my configuration as generic as possible as I intend to distribute it on different systems.

More informations on input methods [right here](https://www.gnu.org/software/emacs/manual/html_node/emacs/Select-Input-Method.html#Select-Input-Method) if you want to have that behaviour as default for instance

I also automated the process to have `M-x accents` available and `M-x current-lang` when I need it:

```elisp
;;Accents
(defun accents ()
  (interactive)
  (activate-input-method "latin-1-alt-postfix")
  )
(defun current-lang ()
  (interactive)
  (eval-expression current-language-environment)
  )
```
([source](http://www.ling.upenn.edu/~beatrice/corpus-ling/enteringAccentedCharacters.html))

Also, make sure that you write and read in UTF-8:

```elisp
;; Use UTF-8 for all character encoding.
(prefer-coding-system 'utf-8)
(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)
(set-selection-coding-system 'utf-8)
(set-locale-environment "en.UTF-8")
(prefer-coding-system 'utf-8)
(setq utf-translate-cjk-mode nil) ; disable CJK coding/encoding
```
