#Display a stacktrace

When debugging with `byebug` and using `binding.pry`, something that I find useful is the `caller` Kernel method.
It gives you the opportunity to display a stacktrace of your current call.

As it gives you an array, I usually use it along `grep` to find exactly what I'm looking for:

```ruby
puts caller.grep /something/
```

For sure, if you arrive at that need, that means that your code is becoming way too complex. But when hitting a bug deep in a library, a gem or a framework, it'll be your best friend to understand better what's going on under the hood.

Source
[http://ruby-doc.org/core-2.2.2/Kernel.html#method-i-caller](http://ruby-doc.org/core-2.2.2/Kernel.html#method-i-caller)
