Sometimes I get curious to know what's going on here. I sometimes read the logs with this command written over time:

```bash
sudo less /var/log/nginx/access.log | grep -v <MY_IP> | \
grep -v '/assets' | grep  -v baidu.com | grep -v YandexBot | \ 
grep -v google.com/bot | grep -v 'ysearch/slurp' | \
grep -v 'http://www.majestic12.co.uk' | grep -v 'webmeup-crawler.com'| \
grep GET | grep -v atom | grep -v css | grep -v favicon.ico
```

Not the smartest, neither the most straightforward. Each time I wanted to remove a line, I added a pipe. Mostly crawlers.
