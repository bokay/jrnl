<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Install the package</a></li>
<li><a href="#sec-2">2. Modify the root jail</a></li>
<li><a href="#sec-3">3. Set loopback for jails</a></li>
<li><a href="#sec-4">4. Install the base jails</a></li>
<li><a href="#sec-5">5. Delete a jail</a></li>
<li><a href="#sec-6">6. Create a jail</a></li>
<li><a href="#sec-7">7. Get a console</a></li>
<li><a href="#sec-8">8. Troubleshoot</a>
<ul>
<li><a href="#sec-8-1">8.1. /etc/sysctl.conf</a></li>
<li><a href="#sec-8-2">8.2. /etc/pf.conf</a>
<ul>
<li><a href="#sec-8-2-1">8.2.1. Run the firewall</a></li>
<li><a href="#sec-8-2-2">8.2.2. Verify the rules</a></li>
</ul>
</li>
<li><a href="#sec-8-3">8.3. IPV6 issues</a></li>
</ul>
</li>
</ul>
</div>
</div>

## Install the ezjails<a id="sec-1" name="sec-1"></a>

`sudo pkg install ezjails`

## Modify the root jail<a id="sec-2" name="sec-2"></a>

`/usr/local/etc/ezjail conf` to jailroot a `/jails`

## Network from inside the jail
`vi /etc/resolvconf`

Add some server to resolve the names

```
nameserver 208.67.222.123
nameserver 208.67.220.123
```

Dns ips found on [opendns.com](https://store.opendns.com/setup/#/familyshield)


## Set loopback for jails<a id="sec-3" name="sec-3"></a>

To keep jail loopback traffic off the host's loopback network interface lo0, a second loopback interface is created by adding an entry to /etc/rc.conf:

```
   cloned_interfaces="lo1"
   ipv4_addrs_lo1="127.0.0.2/900"
```

`service netif cloneup`

## Install the base jails<a id="sec-4" name="sec-4"></a>

```
ezjail-admin install -p
-p for the portsnap
```

## Delete a jail<a id="sec-5" name="sec-5"></a>

```
cd /usr/jails/my_new_jail
chflags -R noschg \*
rm -rf \*
rm /usr/local/etc/ezjail/my_new_jail
```

## Create a jail<a id="sec-6" name="sec-6"></a>

```
ezjail-admin create carnetweb 'lo1|127.0.0.10'
ezjail-admin create carnetweb 'lo1|127.0.0.10,gandiroot'
```

## Get a console<a id="sec-7" name="sec-7"></a>

` ezjail-admin console carnetweb `

## Troubleshoot<a id="sec-8" name="sec-8"></a>

### /etc/sysctl.conf<a id="sec-8-1" name="sec-8-1"></a>

```
net.inet.ip.forwarding=1
net.inet6.ip6.forwarding=1
```

### /etc/pf.conf<a id="sec-8-2" name="sec-8-2"></a>

```
IP_PUB="92.243.17.233"
IP_JAIL="127.0.0.10"
NET_JAIL="127.0.0.0/24"
PORT_JAIL="{80}"
scrub in all
nat pass on vtnet0 from $NET_JAIL to any -> $IP_PUB
rdr pass on vtnet0 proto tcp from any to $IP_PUB port $PORT_JAIL -> $IP_JAIL
```

`pfctl -nf /etc/pf.conf`

#### Run the firewall<a id="sec-8-2-1" name="sec-8-2-1"></a>

`service pf onestart`
or
`service pf start`
or
`service pf restart`

#### Verify the rules<a id="sec-8-2-2" name="sec-8-2-2"></a>

`pfctl -sn`

### IPV6 issues<a id="sec-8-3" name="sec-8-3"></a>

```
ruby -rresolv -e 'puts Resolv::DNS.new.getresource("_rubygems._tcp.rubygems.org", Resolv::DNS::Resource::IN::SRV).target'
/usr/local/lib/ruby/2.1/resolv.rb:748:in \`initialize': Protocol not supported - socket(2) - udp (Errno::EPROTONOSUPPORT)
from /usr/local/lib/ruby/2.1/resolv.rb:748:in \`new'
from /usr/local/lib/ruby/2.1/resolv.rb:748:in \`block in initialize'
from /usr/local/lib/ruby/2.1/resolv.rb:738:in \`each'
from /usr/local/lib/ruby/2.1/resolv.rb:738:in \`initialize'
from /usr/local/lib/ruby/2.1/resolv.rb:562:in \`new'
from /usr/local/lib/ruby/2.1/resolv.rb:562:in \`make_udp_requester'
from /usr/local/lib/ruby/2.1/resolv.rb:517:in \`fetch_resource'
from /usr/local/lib/ruby/2.1/resolv.rb:510:in \`each_resource'
from /usr/local/lib/ruby/2.1/resolv.rb:491:in \`getresource'
from -e:1:in \`<main>'
```

Solution: remove ipv6 ip in /etc/resolv.conf
