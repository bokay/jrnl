I have never been a huge fan of the concept of stocking my passwords in the cloud with any cloud password managers, may it be optional. Recently, in my quest to ditch Evernote from my toolbox, I've made some efforts to get better at org-mode (Emacs).<br/>
Org-mode allows me to group my notes, and thus, passwords by outline. Basically, a tree of headers, sub-headers and content.<br/>
I can now group the passwords by category: work/projects/else.

**Obviously, it would have been a huge mistake if encryption was not part of the deal.**

Hopefully, with emacs, dealing with encrypted GPG files is [so easy that it becomes
handy](http://orgmode.org/worg/org-tutorials/encrypting-files.html).<br/>
When the file is opened, the user gets prompted with a passphrase, and saving the file is totally transparent.

## Pros:
* safe enough for a personnal and daily use
* easy to put together

## Cons:
- presence of emacs mandatory
- all passwords are displayed when the file is opened
- if someone grabs my computer with the file still opened, I'm doomed.
  That's an acceptable risk as I'd close the file once finished.


Update (2015/12/4): <br />
You might want to look for this project, [passbox](https://github.com/RobBollons/passbox). It allows you to manage your gpg password database from the terminal. It's a better solution.
