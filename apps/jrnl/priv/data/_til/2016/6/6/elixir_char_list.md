```
iex(25)> 1..10 |> Enum.reverse |> Enum.take(5)
[10, 9, 8, 7, 6]

iex(26)> 1..10 |> Enum.reverse |> Enum.take(4)
'\n\t\b\a'
```

To display the integers instead of the corresponding chars, run :
```
IEx.configure(inspect: [char_lists: :as_lists])
```
