## IPTABLES
This script will first flush all the rules set, then read the logs, extract the ips making the `/xmlrpc.php` request, reduce the set as unique ips and add for each line a rule to drop the input traffic for these ips.
The flaw with this is that between the moment rules are flushed and the moment we readd them, some requests may pass, but hopefully, not enough to get the server down.

```
#!/bin/bash
/sbin/iptables -F
grep /xmlrpc.php /var/log/apache2/other_vhosts_access.log | cut -d ' ' -f2 | sort | uniq | xargs -n1 /sbin/iptables -A INPUT -j DROP -s
 ```

 We will then transform it as an executable so it can be ran from crontab: `chmod 755 ban_xmlrpc_ips`

 To edit the crontab rules: `crontab -e` and then we can add a rule to watch and ban the ips every 10 minutes:

 ```
 */10 * * * * /path/to/script/ban_xmlrpc_ips
 ```

## FAIL2BAN
This approach is way better.

Install the service `apt-get install fail2ban` on debian.

Edit `/etc/fail2ban/jail.conf` and add this part at the end :

```
[apache-xmlrpc]

enabled  = true
filter   = apache-xmlrpc
action   = iptables[name=WPXMLRPC, port=http, protocol=tcp]
logpath  = /var/log/apache2/other_vhosts_access.log
maxretry = 5
bantime  = -1
```

Note: bantime with a negative value will ban it forever.

Create now the filter ro ban them all located at ` /etc/fail2ban/filter.d/apache-xmlrpc.conf`
Somehow the lines printed in the log were different from what the regex found on the web could find. I needed to also filter the machine ip and the port.

```
[Definition]
failregex = ^[\d|\.]*:\d*.*POST \/xmlrpc\.php .*
ignoreregex =
```

It may take time to start. To know if it's failing or it's just slow, run `fail2ban-regex` like so `fail2ban-regex  /var/log/apache2/other_vhosts_access.log /etc/fail2ban/filter.d/apache-xmlrpc.conf -v > regex_result`

## VERIFY THE RESULTS
- `iptables -L` to check that IPs get banned and with what rule
- `tail -f /var/mail/root` or consult the root mails
- `tail -f /var/log/apache2/other_vhosts_access.log` to check incoming requests
