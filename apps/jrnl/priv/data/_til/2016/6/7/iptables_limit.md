Iptables can handle theorically up to 38 million rules but it gets "flaky" at around 25 000 rules.

That also means that you better use fail2ban for recurrent attacks happening from many different IPs.

Sources :

- [http://serverfault.com/questions/479549/how-many-rules-can-iptables-support#479552](http://serverfault.com/questions/479549/how-many-rules-can-iptables-support#479552)
- [http://www.spinics.net/lists/netfilter/msg51895.html](http://www.spinics.net/lists/netfilter/msg51895.html)
