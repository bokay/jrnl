Applying system security patches is very easy and quick with FreeBSD:


```
# freebsd-update fetch
# freebsd-update install
```

Source:
[https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/updating-upgrading-freebsdupdate.html](https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/updating-upgrading-freebsdupdate.html)
