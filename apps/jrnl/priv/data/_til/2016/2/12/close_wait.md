I've set up a Freebsd on Digital Ocean with jails to host different services:

- gogs.io instance;
- different ruby apps;
- else.

After some time, my requests queued up in the CLOSE_WAIT state:

```bash
root@carnet ~]# netstat
etstat: kvm not available: /dev/mem: No such file or directory
ctive Internet connections
roto Recv-Q Send-Q Local Address          Foreign Address        (state)
cp4     449      0 127.0.0.30.3030        modemcable043.42.55985 ESTABLISHED
cp4     379      0 127.0.0.30.3030        modemcable043.42.55981 CLOSE_WAIT
cp4     449      0 127.0.0.30.3030        modemcable043.42.55917 ESTABLISHED
cp4      18      0 127.0.0.30.3030        127.0.0.30.29129       CLOSE_WAIT
cp4       0      0 127.0.0.30.3030        modemcable043.42.55426 CLOSE_WAIT
cp4       0      0 127.0.0.30.3030        modemcable043.42.55406 CLOSE_WAIT
cp4     395      0 127.0.0.30.3030        modemcable043.42.55405 ESTABLISHED
cp4     492      0 127.0.0.30.3030        modemcable043.42.55383 CLOSED
cp4     492      0 127.0.0.30.3030        modemcable043.42.55382 CLOSE_WAIT
cp4     492      0 127.0.0.30.3030        modemcable043.42.55381 CLOSE_WAIT
cp4       0      0 127.0.0.30.3030        modemcable043.42.55379 CLOSE_WAIT
cp4     492      0 127.0.0.30.3030        modemcable043.42.55380 CLOSE_WAIT
cp4     389      0 127.0.0.30.3030        modemcable043.42.55369 CLOSED
cp4     389      0 127.0.0.30.3030        modemcable043.42.55368 CLOSE_WAIT
cp4     395      0 127.0.0.30.3030        modemcable043.42.55366 CLOSE_WAIT
cp4       0      0 127.0.0.30.3030        modemcable043.42.55364 CLOSE_WAIT
cp4       0      0 127.0.0.30.3030        modemcable043.42.55362 CLOSE_WAIT
cp4       0      0 127.0.0.30.3030        modemcable043.42.55365 CLOSE_WAIT
cp4       0      0 127.0.0.30.3030        modemcable043.42.55363 CLOSE_WAIT
cp4       0      0 127.0.0.30.3030        modemcable043.42.55361 CLOSE_WAIT
cp4     379      0 127.0.0.30.3030        modemcable043.42.55360 CLOSE_WAIT
```

The issue here was because my vhosts didn't take the direct ip into account.
I had:

```
server {
        listen 80;
        server_name niko.bokay.io 159.203.46.1;
```

and adding the public IP fixed the issue for requests done with the IP directly:

```
server {
        listen 80;
        server_name niko.bokay.io 159.203.46.1;
```

Also, I don't know if it actually helped in this issue, but I've launched `rackup` with the `-D` flag so it can run in daemon mode.

My understanding of this issue is that rack was loosing track of the TCP connections to close.
