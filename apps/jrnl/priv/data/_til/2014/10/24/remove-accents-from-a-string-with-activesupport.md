```
> ActiveSupport::Inflector.transliterate 'éòüëêôù'
=> "eoueeou"
```

Source:
[http://api.rubyonrails.org/classes/ActiveSupport/Inflector.html#method-i-transliterate](http://api.rubyonrails.org/classes/ActiveSupport/Inflector.html#method-i-transliterate)