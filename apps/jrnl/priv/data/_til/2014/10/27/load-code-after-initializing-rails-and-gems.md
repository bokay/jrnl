In `application.rb` :

```
config.after_initialize do
  Dir[Rails.root + 'lib/some_extension/**/*.rb'].each do |file|
    require file
  end
end
```