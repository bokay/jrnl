   * **Ping**   
      User `ping6` instead of ping. Generally, network tools for ipv4 are suffixed with a  `6` at the end.
     
   * **SSH**   
     Use `ssh` with `-6` option:    
     Example: `ssh -6 toto@2001:4b98:dd0:51:216:3eff:feb6:5267`
     
   * **An ipv6 address in a browser with a port**   
     Surround the address with square brackets.
     Example:          `http://[2001:4b98:dd0:51:216:3eff:feb6:5267]:8080` for the 8080 port
     
   * **DNS Record**   
   Use a AAAA entry instead of A                  
        Example: `@ 10800 IN AAAA 2001:4b98:dd0:51:216:3eff:feb6:5267`
