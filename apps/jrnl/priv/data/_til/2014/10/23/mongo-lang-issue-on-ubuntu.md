```
Failed global initialization: BadValue Invalid or no user locale set. Please ensure LANG and/or LC_* environment variables are set correctly
```

Solution:

```
sudo locale-gen en_CA en_CA.UTF-8 en_US.UTF-8
```
Source:
[http://askubuntu.com/questions/536875/error-in-installing-mongo-in-virtual-machine](http://askubuntu.com/questions/536875/error-in-installing-mongo-in-virtual-machine)