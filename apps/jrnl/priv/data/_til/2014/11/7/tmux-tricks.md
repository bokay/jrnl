## Sessions
Open tmux with a session name
`tmux new -s session_name`

Rename the session name
`Prefix + $`

List windows and sessions
`tmux ls`

## Panes and windows
Open a window
`Prefix + c`

Open a vertical pane
`Prefix + %`

Open an horizontal pane
`Prefix + "`

Current pane as fullscreen
`Prefix + z`

Close a pane
`Prefix + x`

Close a window
`Prefix + &amp;`

Open an vertical pane
`Prefix + %`

Open an horizontal pane
`Prefix + "`

Navigate through windows
`Prefix + n`
`Prefix + p`

Navigate through panes
`Prefix + <arrows>`

Navigate to previous windows
`Prefix + l`

## Copy/Paste
Enter vim mode (see this as en elevator)
`Prefix + [`
Quit vim mode with `<esc>`

Highlight to copy
`<space> + [h,j,k,l,e,b]`

Yank with `y`

Paste
`Prefix + p`
