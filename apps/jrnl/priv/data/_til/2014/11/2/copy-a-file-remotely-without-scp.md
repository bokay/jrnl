```
cat ~/file | ssh user@server "cat >> ~/file"
```

This can be useful for instance to copy SSH keys from a machine that does not have ssh-copy-id:

```
cat ~/.ssh/id_rsa.pub | ssh user@server 'cat >> .ssh/authorized_keys'
```
