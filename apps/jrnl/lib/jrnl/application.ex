defmodule Jrnl.Application do
  @moduledoc """
  The Jrnl Application Service.

  The jrnl system business domain lives in this application.

  Exposes API to clients such as the `Web` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link([

    ], strategy: :one_for_one, name: Jrnl.Supervisor)
  end
end
