defmodule Jrnl.Category do
  defstruct [:name, :slug, :template, :uuid]
end
