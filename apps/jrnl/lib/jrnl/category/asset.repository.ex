defmodule Jrnl.Asset.Repository do
  def find(category, year, month, day, file_name) do
    root_data = Application.app_dir(:jrnl, "priv/data")

    "#{root_data}/_#{category}/#{year}/#{month}/#{day}/#{file_name}"
    |> Path.wildcard
    |> List.first
  end
end
