defmodule Jrnl.Category.Repository do
  alias Jrnl.Category

  def find_all do
    (Application.app_dir(:jrnl, "priv") <> "/data/categories.yml")
    |> :yamerl_constr.file
    |> List.flatten
    |> List.first
    |> elem(1)
    |> List.flatten
    |> Enum.map(fn({'category', info_list}) ->
      Enum.reduce(info_list, %Category{}, fn {key, value}, acc ->
        Map.put(acc, String.to_atom(to_string(key)), to_string(value))
      end)
    end)
  end

  def find_all_except_cache do
    find_all()
    |> Enum.filter(fn(category) -> category.slug != "cache" end)
  end

  def find(slug) do
    find_all()
    |> Enum.filter(fn(category) -> category.slug == to_string(slug) end)
    |> List.first
  end
end
