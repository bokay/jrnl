defmodule Jrnl.Post.Repository do

  alias Jrnl.Post

  def find_all_by_category_with_content category_slug do
    root_data = Application.app_dir(:jrnl, "priv/data")
    ("#{root_data}/_#{category_slug}/**/*.yml")
    |> Path.wildcard
    |> Enum.map(&build_post_from_path/1)
    |> Enum.reverse
  end

  def build_post_from_path(path) do
    path
    |> :yamerl_constr.file
    |> List.flatten
    |> Enum.reduce( %Post{}, fn {key, value}, acc_post ->
      cond do
        is_list(value) ->
          Map.put(acc_post, String.to_atom(to_string(key)), value)
        true ->
          Map.put(acc_post, String.to_atom(to_string(key)), IO.iodata_to_binary(value))
      end
    end)
    |> complete_with_metadata(path)
    |> complete_with_content(path)
  end

  defp complete_with_metadata(post, path) do
    regex = ~r/data\/_(.*)\/(\d*)\/(\d*)\/(\d*)\/(.*).yml/
    [ _prefix | metadata] = Regex.scan(regex, path) |> List.flatten

    year = Enum.at(metadata, 1) |> String.to_integer()
    month = Enum.at(metadata, 2) |> String.to_integer()
    day = Enum.at(metadata, 3) |> String.to_integer()

    post
    |> Map.put(:category, Enum.at(metadata, 0))
    |> Map.put(:year, year)
    |> Map.put(:month, month)
    |> Map.put(:day, day)
    |> Map.put(:slug, Enum.at(metadata, 4))
    |> Map.put(:timestamp_s, "#{year}/#{month}/#{day}")
    |> Map.put(:timestamp, Date.new(year, month, day))
  end

  defp complete_with_content(post, yml_path) do
    md_path = String.replace_suffix(yml_path, "yml", "md")
    case File.read(md_path) do
      {:ok, content} -> Map.put(post, :content, content)
      {:error, _reason} -> post
    end
  end

  def find_sorted_by_year(category_slug) do
    category_slug
    |> find_all_by_category_with_content
    |> Enum.reduce(Keyword.new, fn(post, acc) ->
      year =  String.to_atom("#{post.year}")
      list = List.wrap(acc[year]) ++ [post]
      Keyword.put(acc, year, Enum.reverse(list))
    end)
    |> Enum.reverse
  end

  def find(category, year, month, day, slug) do
    root_data = Application.app_dir(:jrnl, "priv/data")
    yml_path = Path.wildcard("#{root_data}/_#{category}/#{year}/#{month}/#{day}/#{slug}.yml")

    build_post_from_path(List.first(yml_path))
  end
end
