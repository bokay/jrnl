defmodule Jrnl.Post do
  defstruct [:title, :location, :id, :updated_at, :created_at]

  def uri(post) do
    "/categories/#{post.category}/posts/#{post.timestamp_s}/#{post.slug}"
  end
end
