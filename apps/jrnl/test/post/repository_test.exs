defmodule Jrnl.Post.RepositoryTest do
  use ExUnit.Case

  alias Jrnl.Post

  describe "finding all post for a given category" do
    test "returns all posts" do
      posts = Jrnl.Post.Repository.find_all_by_category_with_content :dev
      assert is_list(posts)
      first_post = List.last(posts)
      assert first_post.__struct__ == Post
      IO.inspect posts
    end
  end

  describe "sorting by year" do
    test "contains posts sorted by year" do
      posts_by_year = Jrnl.Post.Repository.find_sorted_by_year :til
      assert :"2014" in Keyword.keys(posts_by_year)
      assert is_list posts_by_year[:"2014"]
      assert List.first(posts_by_year[:"2014"]).__struct__ == Post
    end
  end

  describe "finding a post" do
    test "a date and a slug returns the corresponding post" do
      category = "dev"
      day = "9"
      month = "3"
      slug = "scrumban"
      year = "2016"

      post = Jrnl.Post.Repository.find(category, year,month,day, slug)
      assert post.__struct__ == Post
      end
  end
end
