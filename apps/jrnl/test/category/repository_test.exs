defmodule Jrnl.Category.RepositoryTest do
  use ExUnit.Case

  alias Jrnl.Category

  describe "finding all categories" do
    test "returns all posts" do
      categories = Jrnl.Category.Repository.find_all
      assert is_list(categories)
      first_category = List.first(categories)
      assert first_category.__struct__ == Category
    end

    test "returns all posts except cache" do
      categories = Jrnl.Category.Repository.find_all_except_cache
      assert is_list(categories)
      first_category = List.first(categories)
      assert first_category.__struct__ == Category
      cache_list = Enum.filter(categories, fn(category) -> category.slug == "cache" end)
      assert Enum.empty?(cache_list)

    end
  end

  describe "finding a specific category" do
    test "returns all posts" do
      category = Jrnl.Category.Repository.find(:ego)
      assert is_map(category)
      assert category.__struct__ == Category
    end
  end
end
