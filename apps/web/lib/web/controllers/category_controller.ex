defmodule Web.CategoryController do
  use Web, :controller

  def index(conn, %{"category" => "dev"}) do
    category = Jrnl.Category.Repository.find(:dev)
    posts_by_year = Jrnl.Post.Repository.find_sorted_by_year(category.slug)

    render conn,
      "dev.html",
      category: category,
      posts_by_year: posts_by_year
  end

  def index(conn, %{"category" => "ego"}) do
    category = Jrnl.Category.Repository.find(:ego)
    posts = Jrnl.Post.Repository.find_all_by_category_with_content(category.slug)

    render conn, "ego.html", category: category, posts: posts
  end

  def index(conn, %{"category" => "til"}) do
    category = Jrnl.Category.Repository.find(:til)
    posts = Jrnl.Post.Repository.find_all_by_category_with_content(category.slug)

    render conn, "til.html", category: category, posts: posts
  end

  def atom(conn, %{"category" => category_slug}) do
    posts = Jrnl.Post.Repository.find_all_by_category_with_content(category_slug)
    category = Jrnl.Category.Repository.find(category_slug)

    conn
    |> put_resp_content_type("application/atom+xml")
    |> text(Web.FeedBuilder.build(category, posts))
  end
end
