defmodule Web.PostController do
  use Web, :controller

  def show(conn, %{
        "category" => category_slug,
        "year" => year,
        "month" => month,
        "day" => day,
        "slug" => slug
      }) do
    category = Jrnl.Category.Repository.find(category_slug)
    categories = Jrnl.Category.Repository.find_all_except_cache()
    post = Jrnl.Post.Repository.find(category_slug, year, month, day, slug)

    render(
      conn,
      "show.html",
      category: category,
      categories: categories,
      post: post,
      title: post.title
    )
  end

  def show_json(conn, %{
        "category" => category_slug,
        "year" => year,
        "month" => month,
        "day" => day,
        "slug" => slug
      }) do
    post = Jrnl.Post.Repository.find(category_slug, year, month, day, slug)

    json(conn, %{
      content: Earmark.as_html!(post.content),
      title: to_string(post.title),
      date: post.timestamp_s,
      uri: Jrnl.Post.uri(post)
    })
  end

  def asset(conn, %{
        "category" => category,
        "year" => year,
        "month" => month,
        "day" => day,
        "filename" => file_name
      }) do
    path = Jrnl.Asset.Repository.find(category, year, month, day, file_name)
    Plug.Conn.send_file(conn, 200, path)
  end
end
