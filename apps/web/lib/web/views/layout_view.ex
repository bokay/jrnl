defmodule Web.LayoutView do
  use Web, :view

  def menu_selected(conn, :dev) do
    if String.contains?(conn.request_path, "categories/dev") do
      "menu_selected"
    end
  end

  def menu_selected(conn, :ego) do
    if String.contains?(conn.request_path, "categories/ego") || String.contains?(conn.request_path, "categories/journal") do
      "menu_selected"
    end
  end

  def menu_selected(conn, :toolbox) do
    if String.contains?(conn.request_path, "categories/til") do
      "menu_selected"
    end
  end

  def menu_selected(conn, :now) do
    if String.contains?(conn.request_path, "now") do
      "menu_selected"
    end
  end

  def menu_selected(conn, :home) do
    if conn.request_path == "/" do
      "menu_selected"
    end
  end
end
