defmodule Web.CategoryView do
  use Web, :view
  import Web.PostView, only: [format: 1]

  def first_post_as_list(posts_by_year) do
    posts_by_year
    |>List.first
    |> elem(1)
    |> List.first
    |> List.wrap
  end
end
