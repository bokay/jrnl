defmodule Web.PostView do
  use Web, :view

  def format(markdown) do
    Earmark.as_html!(markdown)
  end
end
