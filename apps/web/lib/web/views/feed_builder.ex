defmodule Web.FeedBuilder do
  alias Atomex.{Feed, Entry}

  def build(category, posts) do
    last_comment_published = List.first(posts)

    feed_id = "urn:uuid:#{category.uuid}"
    last_update = find_created_at(last_comment_published)

    Feed.new(feed_id, last_update, "Bokay.io")
    |> Feed.author("Niko Charlery", email: "nicolas@bokay.io")
    |> Feed.link("http://niko.bokay.io/categories/#{category.slug}/posts.atom", rel: "self")
    |> Feed.entries(Enum.map(posts, &get_entry(category, &1)))
    |> Feed.build()
    |> Atomex.generate_document()
  end

  defp get_entry(category, post) do
    content = Earmark.as_html!(post.content)
    url = "http://niko.bokay.io/categories/#{category.slug}/#{post.timestamp_s}/#{post.slug}"
    post_id = "urn:uuid:#{post.id}"

    Entry.new(post_id, find_created_at(post), to_string(post.title))
    |> Entry.author("Nicolas Charlery", uri: "http://niko.bokay.io")
    |> Entry.content(content, type: "html")
    |> Entry.add_field(:link, %{href: url}, nil)
    |> Entry.build()
  end

  defp find_created_at(post) do
    date_ary = String.split(post.timestamp_s, "/")
    year = Enum.at(date_ary, 0)
    month = Enum.at(date_ary, 1) |> String.pad_leading(2, "0")
    day = Enum.at(date_ary, 2) |> String.pad_leading(2, "0")
    date_s = "#{year}-#{month}-#{day}"
    {:ok, created_at, 0} = DateTime.from_iso8601("#{date_s}T00:00:07Z")

    created_at
  end
end
