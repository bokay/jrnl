// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html"

function makeRequest(url) {
    let httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        window.location = url;
        return false;
    }
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                let json = JSON.parse(httpRequest.responseText);
                var mq = window.matchMedia( "(min-width: 950px)" );
                if (mq.matches) {
                    displayArticle(json);
                } else {
                    window.location = json.uri;
                }
            }
        }
    }

    httpRequest.open('GET', url);
    httpRequest.send();
    return true;
}

function displayArticle(json) {
    displayContent(json.content);
    displayDate(json.date, json.uri);
    displayTitle(json.title, json.uri);
}

function displayTitle(title, uri) {
    var output = "<a href='";
    output = output + uri + "'>";
    output = output + title + "</a>";
    document.getElementById("js-pl-title").innerHTML = output;
}

function displayDate(date, uri) {
    document.getElementById("js-pl-date").innerHTML = date;
}

function displayContent(content) {
    document.getElementById("js-pl-content").innerHTML = content;
}


document.onreadystatechange = function () {
    let links = document.getElementsByClassName('js-remote-link');
    let index;
    for (index = 0; index < links.length; ++index) {
        let link = links[index];
        link.addEventListener("click", function(e){
            e.preventDefault();
            e.stopPropagation();

            let remote_url = e.target.getAttribute('data-url');
            makeRequest(remote_url);
        });
    }
};
