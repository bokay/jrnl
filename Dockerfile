### BUILD STAGE
FROM elixir:1.10.4-alpine as builder
RUN mkdir /app

WORKDIR /app

# Hex and rebar can be out of sync with the base docker image
RUN mix local.hex --force
RUN mix local.rebar --force


# Install asdf and nodejs
RUN apk update
RUN apk add --update nodejs npm git

# Provide a default for the MIX_ENV, see heroku.yml for more information
ARG MIX_ENV=prod
ENV MIX_ENV ${MIX_ENV}
RUN echo $MIX_ENV

# Umbrella
COPY mix.exs mix.lock ./
COPY config config

# Apps
COPY apps apps
RUN mix do deps.get, deps.compile

WORKDIR /app/apps/web
RUN rm -rf priv/static/*

# Build assets in production mode:
WORKDIR /app/apps/web/assets

# Fix for `Error: could not get uid/gid`
#RUN npm config set unsafe-perm true

# The version 6.0.1 returned "npm ERR! write after end" sometimes
# https://github.com/npm/npm/issues/19989
#RUN npm i -g npm@6.1.0

# Remove any existing node modules that exist from the host platform. This will cause problems
# if we have modules from the host with OS specific extensions.
RUN rm -rf ./node_modules/*
RUN npm install

ENV NODE_ENV=production
RUN ./node_modules/webpack/bin/webpack.js -p --mode=production


WORKDIR /app/apps/web
RUN mix phx.digest

WORKDIR /app
#COPY rel rel
RUN MIX_ENV=prod mix release web


### RELEASE STAGE
FROM alpine:3.11

# we need bash and openssl for Phoenix
RUN apk update \
    apk upgrade --no-cache && \
    apk add --no-cache bash openssl

EXPOSE 5010

 # Provide a default for the MIX_ENV, see heroku.yml for more information
ARG MIX_ENV=prod
ENV PORT=5010 \
    MIX_ENV=prod \
    NODE_ENV=production \
    SHELL=/bin/bash

RUN mkdir /app
WORKDIR /app

COPY --from=builder /app/_build/$MIX_ENV/rel/web .

RUN chown -R root ./releases

USER root

CMD ["/app/bin/web", "start"]
